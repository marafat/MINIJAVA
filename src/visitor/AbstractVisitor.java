package visitor;

import ast.*;
import ast.expression.*;

/**
 * @author marafat
 */

public abstract class AbstractVisitor<R> {

    public R visit(final Program node) {
        node.mc.accept(this);
        for (final ClassDeclaration c: node.cd)
            if (c != null)
                c.accept(this);
        return null;
    }

    public R visit(final MainClass node) {
        node.id.accept(this);
        node.mmd.accept(this);
        return null;
    }

    public R visit(final MainMethodDeclaration node) {
        node.id.accept(this);
        node.mmb.accept(this);
        return null;
    }

    public R visit(final MainMethodBody node) {
        node.s.accept(this);
        return null;
    }

    public R visit(final ClassDeclarationSimple node) {
        node.id.accept(this);
        node.cb.accept(this);
        return null;
    }

    public R visit(final ClassDeclarationExtends node) {
        node.ids.accept(this);
        node.idx.accept(this);
        node.cb.accept(this);
        return null;
    }

    public R visit(final ClassBody node) {
        for (final VarDeclaration v: node.vd)
            if (v != null)
                v.accept(this);

        for (final MethodDeclaration m: node.md)
            if (m != null)
                m.accept(this);

        return null;
    }

    public R visit(final VarDeclaration node) {
        node.t.accept(this);
        node.id.accept(this);
        return null;
    }

    public R visit(final MethodDeclaration node) {
        node.t.accept(this);
        node.id.accept(this);
        if (node.pl != null)
            node.pl.accept(this);
        node.mb.accept(this);
        return null;
    }

    public R visit(final ParameterList node) {
        for (final Type t: node.tl)
            if (t != null)
                t.accept(this);

        for (final Identifier i: node.idl)
            if (i != null)
                i.accept(this);

        return null;
    }

    public R visit(final MethodBody node) {
        for (final VarDeclaration v: node.vdl)
            if (v != null)
                v.accept(this);

        for (final Statement s: node.sl)
            if (s != null)
                s.accept(this);

        node.re.accept(this);
        return null;
    }

    public R visit(final IntArrayType node) {
        return null;
    }

    public R visit(final BooleanType node) {
        return null;
    }

    public R visit(final IntegerType node) {
        return null;
    }

    public R visit(final IdentifierType node) {
        return null;
    }

    public R visit(final Block node) {
        for (final Statement s: node.sl)
            if (s != null)
                s.accept(this);

        return null;
    }

    public R visit(final IfStatement node) {
        node.e.accept(this);
        node.s1.accept(this);
        node.s2.accept(this);
        return null;
    }

    public R visit(final WhileStatement node) {
        node.e.accept(this);
        node.s.accept(this);
        return null;
    }

    public R visit(final PrintStatement node) {
        node.e.accept(this);
        return null;
    }

    public R visit(final AssignStatement node) {
        node.id.accept(this);
        node.e.accept(this);
        return null;
    }

    public R visit(final ArrayAssignStatement node) {
        node.id.accept(this);
        node.e1.accept(this);
        node.e2.accept(this);
        return null;
    }

    public R visit(final LogicalExpression node) {
        node.le.accept(this);
        node.re.accept(this);
        return null;
    }

    public R visit(final CompareExpression node) {
        node.le.accept(this);
        node.re.accept(this);
        return null;
    }

    public R visit(final ArithmeticExpression node) {
        node.le.accept(this);
        node.re.accept(this);
        return null;
    }

    public R visit(final ArrayLookupExpression node) {
        node.ref.accept(this);
        node.ss.accept(this);
        return null;
    }

    public R visit(final ArrayLengthExpression node) {
        node.ref.accept(this);
        return null;
    }

    public R visit(final CallExpression node) {
        node.ref.accept(this);
        node.id.accept(this);
        for (Expression e: node.al)
            e.accept(this);
        return null;
    }

    public R visit(final NewArrayExpression node) {
        node.size.accept(this);
        return null;
    }

    public R visit(final NewObjectExpression node) {
        node.id.accept(this);
        return null;
    }

    public R visit(final NotExpression node) {
        node.e.accept(this);
        return null;
    }

    public R visit(final ParenthesisExpression node) {
        node.e.accept(this);
        return null;
    }

    public R visit(final IntegerLiteral node) {
        return null;
    }

    public R visit(final BooleanTrueLiteral node) {
        return null;
    }

    public R visit(final BooleanFalseLiteral node) {
        return null;
    }

    public R visit(final ThisExpression node) {
        return null;
    }

    public R visit(final IdentifierExpression node) {
        return null;
    }

    public R visit(final Identifier node) {
        return null;
    }
}
