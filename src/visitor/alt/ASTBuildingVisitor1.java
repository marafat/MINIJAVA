package visitor.alt;

import ast.*;
import ast.expression.*;
import frontend.MiniJavaBaseVisitor;
import frontend.MiniJavaParser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author marafat
 */

public class ASTBuildingVisitor1 extends MiniJavaBaseVisitor<Node> {

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitProgram(MiniJavaParser.ProgramContext ctx) {
        final List<ClassDeclaration> cdl = new ArrayList<>();
        final MainClass mc = (MainClass) ctx.mainClass().accept(this);

        for (final MiniJavaParser.ClassDeclarationContext cdc: ctx.classDeclaration())
            if (cdc != null)
                cdl.add((ClassDeclaration) cdc.accept(this));

        return new Program(mc, cdl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMainClass(MiniJavaParser.MainClassContext ctx) {
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        final MainMethodDeclaration mmd = (MainMethodDeclaration) ctx.mainMethodDeclaration().accept(this);
        return new MainClass(id, mmd, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMainMethodDeclaration(MiniJavaParser.MainMethodDeclarationContext ctx) {
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        final MainMethodBody mmb = (MainMethodBody) ctx.mainMethodBody().accept(this);
        return new MainMethodDeclaration(id, mmb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMainMethodBody(MiniJavaParser.MainMethodBodyContext ctx) {
        final Statement s = (Statement) visit(ctx.statement());
        return new MainMethodBody(s, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassDeclaration(MiniJavaParser.ClassDeclarationContext ctx) {
        return super.visitClassDeclaration(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassDeclarationSimple(MiniJavaParser.ClassDeclarationSimpleContext ctx) {
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        final ClassBody cb = (ClassBody) ctx.classBody().accept(this);
        return new ClassDeclarationSimple(id, cb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassDeclarationExtends(MiniJavaParser.ClassDeclarationExtendsContext ctx) {
        final Identifier simple = (Identifier) ctx.identifier(0).accept(this);
        final Identifier extend = (Identifier) ctx.identifier(1).accept(this);
        final ClassBody cb = (ClassBody) ctx.classBody().accept(this);
        return new ClassDeclarationExtends(simple, extend, cb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassBody(MiniJavaParser.ClassBodyContext ctx) {
        final List<VarDeclaration> vdl = new ArrayList<>();
        for (final MiniJavaParser.VarDeclarationContext v: ctx.varDeclaration())
            if (v != null)
                vdl.add((VarDeclaration) v.accept(this));

        final List<MethodDeclaration> mdl = new ArrayList<>();
        for (final MiniJavaParser.MethodDeclarationContext m : ctx.methodDeclaration())
            if (m != null)
                mdl.add((MethodDeclaration) m.accept(this));

        return new ClassBody(vdl, mdl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitVarDeclaration(MiniJavaParser.VarDeclarationContext ctx) {
        final Type t = (Type) ctx.type().accept(this);
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        return new VarDeclaration(t, id, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMethodDeclaration(MiniJavaParser.MethodDeclarationContext ctx) {
        final Type t = (Type) visit(ctx.type());
        final Identifier id = (Identifier) visit(ctx.identifier());
        ParameterList pl = null;
        if (ctx.parameterList() != null)
            pl = (ParameterList) ctx.parameterList().accept(this);
        final MethodBody mb = (MethodBody) ctx.methodBody().accept(this);
        return new MethodDeclaration(t, id, pl, mb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitParameterList(MiniJavaParser.ParameterListContext ctx) {
        final List<Type> tl = new ArrayList<>();
        for(final MiniJavaParser.TypeContext t: ctx.type())
            if (t != null)
                tl.add((Type) t.accept(this));

        final List<Identifier> idl = new ArrayList<>();
        for (final MiniJavaParser.IdentifierContext id: ctx.identifier())
            if (id != null)
                idl.add((Identifier) id.accept(this));

        return new ParameterList(tl, idl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMethodBody(MiniJavaParser.MethodBodyContext ctx) {
        final List<VarDeclaration> vdl = new ArrayList<>();
        for (final MiniJavaParser.VarDeclarationContext vd: ctx.varDeclaration())
            if (vd != null)
                vdl.add((VarDeclaration) vd.accept(this));

        final List<Statement> sl = new ArrayList<>();
        for (final MiniJavaParser.StatementContext s: ctx.statement())
            if (s != null)
                sl.add((Statement) s.accept(this));

        final Expression e = (Expression) ctx.expression().accept(this);

        return new MethodBody(vdl, sl, e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitType(MiniJavaParser.TypeContext ctx) {
        return super.visitType(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIntArrayType(MiniJavaParser.IntArrayTypeContext ctx) {
        return new IntArrayType(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBooleanType(MiniJavaParser.BooleanTypeContext ctx) {
        return new BooleanType(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIntegerType(MiniJavaParser.IntegerTypeContext ctx) {
        return new IntegerType(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIdentifierType(MiniJavaParser.IdentifierTypeContext ctx) {
        return new IdentifierType(ctx.ID().getText(), ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitStatement(MiniJavaParser.StatementContext ctx) {
        return super.visitStatement(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBlock(MiniJavaParser.BlockContext ctx) {
        final List<Statement> sl = new ArrayList<>();
        for (final MiniJavaParser.StatementContext s: ctx.statement())
            if (s != null)
                sl.add((Statement) s.accept(this));

        return new Block(sl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIfStatement(MiniJavaParser.IfStatementContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        final Statement s1 = (Statement) ctx.statement(0).accept(this);
        final Statement s2 = (Statement) ctx.statement(1).accept(this);
        return new IfStatement(e, s1, s2, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitWhileStatement(MiniJavaParser.WhileStatementContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        final Statement s1 = (Statement) ctx.statement().accept(this);
        return new WhileStatement(e, s1, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitPrintStatement(MiniJavaParser.PrintStatementContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        return new PrintStatement(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitAssignStatement(MiniJavaParser.AssignStatementContext ctx) {
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        final Expression e = (Expression) ctx.expression().accept(this);
        return new AssignStatement(id, e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArrayAssignStatement(MiniJavaParser.ArrayAssignStatementContext ctx) {
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        final Expression e1 = (Expression) ctx.expression(0).accept(this);
        final Expression e2 = (Expression) ctx.expression(1).accept(this);
        return new ArrayAssignStatement(id, e1, e2, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitParenthesisExpression(MiniJavaParser.ParenthesisExpressionContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        return new ParenthesisExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArrayLookupExpression(MiniJavaParser.ArrayLookupExpressionContext ctx) {
        final Expression e1 = (Expression) ctx.expression(0).accept(this);
        final Expression e2 = (Expression) ctx.expression(1).accept(this);
        return new ArrayLookupExpression(e1, e2, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitNotExpression(MiniJavaParser.NotExpressionContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        return new NotExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitCompareExpression(MiniJavaParser.CompareExpressionContext ctx) {
        final Expression l = (Expression) ctx.expression(0).accept(this);
        int op = ctx.op.getType();
        final Expression r = (Expression) ctx.expression(1).accept(this);
        return new CompareExpression(l, op, r, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArithmeticExpression(MiniJavaParser.ArithmeticExpressionContext ctx) {
        final Expression l = (Expression) ctx.expression(0).accept(this);
        int op = ctx.op.getType();
        final Expression r = (Expression) ctx.expression(1).accept(this);
        return new ArithmeticExpression(l, op, r, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitNewObjectExpression(MiniJavaParser.NewObjectExpressionContext ctx) {
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        return new NewObjectExpression(id, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIdentifierExpression(MiniJavaParser.IdentifierExpressionContext ctx) {
        return new IdentifierExpression(ctx.ID().getText(), ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitThisExpression(MiniJavaParser.ThisExpressionContext ctx) {
        return new ThisExpression(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitLogicalExpression(MiniJavaParser.LogicalExpressionContext ctx) {
        final Expression l = (Expression) ctx.expression(0).accept(this);
        int op = ctx.op.getType();
        final Expression r = (Expression) ctx.expression(1).accept(this);
        return new LogicalExpression(l, op, r, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArrayLengthExpression(MiniJavaParser.ArrayLengthExpressionContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        return new ArrayLengthExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBooleanTrueLiteral(MiniJavaParser.BooleanTrueLiteralContext ctx) {
        return new BooleanTrueLiteral(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitNewArrayExpression(MiniJavaParser.NewArrayExpressionContext ctx) {
        final Expression e = (Expression) ctx.expression().accept(this);
        return new NewArrayExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitCallExpression(MiniJavaParser.CallExpressionContext ctx) {
        final Expression e = (Expression) ctx.expression(0).accept(this);
        final Identifier id = (Identifier) ctx.identifier().accept(this);
        List<Expression> al = new ArrayList<>();
        for (int i = 1; i < ctx.expression().size(); i++) {
            al.add((Expression) visit(ctx.expression(i)));
        }

        return new CallExpression(e, id, al, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIntegerLiteral(MiniJavaParser.IntegerLiteralContext ctx) {
        int value = Integer.parseInt(ctx.INTEGER_LITERAL().getText());
        return new IntegerLiteral(value, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBooleanFalseLiteral(MiniJavaParser.BooleanFalseLiteralContext ctx) {
        return new BooleanFalseLiteral(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIdentifier(MiniJavaParser.IdentifierContext ctx) {
        return new Identifier(ctx.ID().getText(), ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }
}
