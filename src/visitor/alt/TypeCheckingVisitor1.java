package visitor.alt;

import ast.*;
import ast.expression.*;
import error.Error;
import symbol.*;
import visitor.AbstractVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author marafat
 */

public class TypeCheckingVisitor1 extends AbstractVisitor<Type> {

    private TableSemantic symTable;
    private Error error;
    private Symbol currentClass;
    private MethodDeclaration currentMethod;

    public TypeCheckingVisitor1(final Map<Symbol, ClassBinding> forwardDecls) {
        symTable = new TableSemantic(forwardDecls);
        error = new Error();
    }

    public void traverse(final Node ast) {
        ast.accept(this);
        if (error.getCount() > 0)
            System.exit(1);
    }

    @Override
    public Type visit(final Program node) {
        return super.visit(node);
    }

    @Override
    public Type visit(final MainClass node) {
        symTable.enterScope();
        node.mmd.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Type visit(final MainMethodDeclaration node) {
        symTable.enterScope();
        node.mmb.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Type visit(final MainMethodBody node) {
        return super.visit(node);
    }

    @Override
    public Type visit(final ClassDeclarationSimple node) {
        currentClass = Symbol.symbol(node.id.n);
        symTable.enterScope();
        node.cb.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Type visit(final ClassDeclarationExtends node) {
        currentClass = Symbol.symbol(node.ids.n);
        symTable.enterScope();
        if (!symTable.insertSuperclassField(Symbol.symbol(node.idx.n)))
            error.display(node.idx.line, node.idx.column, "undefined symbol '" + node.idx.n + "'");

        node.cb.accept(this);
        symTable.exitScope();

        return null;
    }

    @Override
    public Type visit(final ClassBody node) {
        for (final VarDeclaration v: node.vd)
            if (v != null)
                symTable.insertField(Symbol.symbol(v.id.n), v.t);

        for (final MethodDeclaration m: node.md)
            if (m != null)
                m.accept(this);

        return null;
    }

    @Override
    public Type visit(final VarDeclaration node) {
        node.t.accept(this);
        if (!symTable.insert(Symbol.symbol(node.id.n), node.t))
            error.display(node.id.line, node.id.column, "variable '" + node.id.n + "' already exists");

        return null;
    }

    @Override
    public Type visit(final MethodDeclaration node) {
        currentMethod = node;
        symTable.enterScope();
        node.t.accept(this);
        if (node.pl != null)
            node.pl.accept(this);
        node.mb.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Type visit(final ParameterList node) {
        for (int i = 0; i < node.tl.size(); i++) {
            node.tl.get(i).accept(this);
            if (!symTable.insert(Symbol.symbol(node.idl.get(i).n), node.tl.get(i)))
                error.display(node.idl.get(i).line, node.idl.get(i).column, "variable '" + node.idl.get(i).n + "' already exists");
        }

        return null;
    }

    @Override
    public Type visit(final MethodBody node) {
        for (final VarDeclaration v: node.vdl)
            if (v != null)
                v.accept(this);

        for (final Statement s: node.sl)
            if (s != null)
                s.accept(this);

        final Type actualReturnType = node.re.accept(this);
        final Type declaredReturnType;
        if (currentMethod.pl != null)
            declaredReturnType = symTable.getReturnType(currentClass, Symbol.symbol(currentMethod.id.n), currentMethod.pl.tl);
        else
            declaredReturnType = symTable.getReturnType(currentClass, Symbol.symbol(currentMethod.id.n), new ArrayList<Type>());

        if (!actualReturnType.toString().equals(declaredReturnType.toString()))
            error.display(node.re.line, node.re.column, "return type does not match the method's return type");

        return null;
    }

    @Override
    public Type visit(final IntArrayType node) {
        return node;
    }

    @Override
    public Type visit(final BooleanType node) {
        return node;
    }

    @Override
    public Type visit(final IntegerType node) { return node; }

    @Override
    public Type visit(final IdentifierType node) {
        if (!symTable.typeExist(Symbol.symbol(node.n)))
            error.display(node.line, node.column, "undefined symbol '" + node.n + "'");

        return node;
    }

    @Override
    public Type visit(final Block node) {
        symTable.enterScope();
        super.visit(node);
        symTable.exitScope();
        return null;
    }

    @Override
    public Type visit(final IfStatement node) {
        final Type t = node.e.accept(this);
        if (!t.toString().equals("BOOLEAN"))
            error.display(node.e.line, node.e.column, "Boolean expression expected");

        node.s1.accept(this);
        node.s2.accept(this);

        return null;
    }

    @Override
    public Type visit(final WhileStatement node) {
        final Type t = node.e.accept(this);
        if (!t.toString().equals("BOOLEAN"))
            error.display(node.e.line, node.e.column, "Boolean expression expected");

        node.s.accept(this);

        return null;
    }

    @Override
    public Type visit(final PrintStatement node) {
        final Type t = node.e.accept(this);
        if (!t.toString().equals("INTEGER"))
            error.display(node.e.line, node.e.column, "Integer expression expected");

        return null;
    }

    @Override
    public Type visit(final AssignStatement node) {
        final Type lhs = symTable.lookup(Symbol.symbol(node.id.n));
        if (lhs == null) {
            error.display(node.id.line, node.id.column, "undefined symbol '" + node.id.n + "'");
            return null;
        }

        final Type rhs = node.e.accept(this);
        if (!lhs.toString().equals(rhs.toString()))
            if (!symTable.isSuperClass(Symbol.symbol(lhs.toString()), Symbol.symbol(rhs.toString())))
                error.display(node.e.line, node.e.column, "can not assign " + rhs.toString() + " to " + lhs.toString());

        return null;
    }

    @Override
    public Type visit(final ArrayAssignStatement node) {
        final Type t = symTable.lookup(Symbol.symbol(node.id.n));
        if (t == null)
            error.display(node.id.line, node.id.column, "undefined symbol '" + node.id.n + "'");
        else if (!t.toString().equals("INTARRAY"))
            error.display(node.id.line, node.id.column, "'" + node.id.n + "' can not be used as IntArray");

        final Type t1 = node.e1.accept(this);
        if (!t1.toString().equals("INTEGER"))
            error.display(node.e1.line, node.e1.column, "array access type should be integer");

        final Type t2 = node.e2.accept(this);
        if (!t2.toString().equals("INTEGER"))
            error.display(node.e2.line, node.e2.column, "can not assign " + t2.toString() + " to " + "integer");

        return null;
    }

    @Override
    public Type visit(final LogicalExpression node) {
        final Type lhs = node.le.accept(this);
        if (!lhs.toString().equals("BOOLEAN"))
            error.display(lhs.line, lhs.column, "Boolean expression expected");

        final Type rhs = node.re.accept(this);
        if (!rhs.toString().equals("BOOLEAN"))
            error.display(rhs.line, rhs.column, "Boolean expression expected");

        return new BooleanType(node.le.line, node.le.column);
    }

    @Override
    public Type visit(final CompareExpression node) {
        final Type lhs = node.le.accept(this);
        if (!lhs.toString().equals("INTEGER"))
            error.display(lhs.line, lhs.column, "Integer expression expected");

        final Type rhs = node.re.accept(this);
        if (!rhs.toString().equals("INTEGER"))
            error.display(rhs.line, rhs.column, "Integer expression expected");

        return new BooleanType(node.le.line, node.le.column);
    }

    @Override
    public Type visit(final ArithmeticExpression node) {
        final Type lhs = node.le.accept(this);
        if (!lhs.toString().equals("INTEGER"))
            error.display(lhs.line, lhs.column, "Integer expression expected");

        final Type rhs = node.re.accept(this);
        if (!rhs.toString().equals("INTEGER"))
            error.display(rhs.line, rhs.column, "Integer expression expected");

        return new IntegerType(node.le.line, node.le.column);
    }

    @Override
    public Type visit(final ArrayLookupExpression node) {
        final Type t1 = node.ref.accept(this);
        if (!t1.toString().equals("INTARRAY"))
            error.display(node.ref.line, node.ref.column, "IntArray expression expected");

        final Type t2 = node.ss.accept(this);
        if (!t2.toString().equals("INTEGER"))
            error.display(node.ss.line, node.ss.column, "Integer expression expected");

        return new IntegerType(node.ref.line, node.ref.column);
    }

    @Override
    public Type visit(final ArrayLengthExpression node) {
        final Type t = node.ref.accept(this);
        if (!t.toString().equals("INTARRAY")) {
            error.display(node.ref.line, node.ref.column, "length applicable to array types only");
        }

        return new IntegerType(node.ref.line, node.ref.column);
    }

    @Override
    public Type visit(final CallExpression node) {
        final List<Type> params = new ArrayList<>();
        if (node.al != null) {
            for (Expression e : node.al) {
                Type t = e.accept(this);
                if (t == null)
                    return null;
                params.add(t);
            }
        }

        Type ret = null;
        final Type rcv = node.ref.accept(this);
        if (!rcv.toString().equals("IDENTIFIER") || !symTable.typeExist(Symbol.symbol(((IdentifierType) rcv).n)))
            error.display(node.id.line, node.id.column, "undefined symbol '" + node.id.n + "'");
        else
            ret = symTable.getReturnType(Symbol.symbol(((IdentifierType)rcv).n), Symbol.symbol(node.id.n), params);

        if (ret == null) {
            error.display(node.id.line, node.id.column, "method '" + node.id.n + "' does not exist");
            return new IdentifierType("", node.id.line, node.id.column);
        }

        return ret;
    }

    @Override
    public Type visit(final IntegerLiteral node) {
        return new IntegerType(node.line, node.column);
    }

    @Override
    public Type visit(final BooleanTrueLiteral node) {
        return new BooleanType(node.line, node.column);
    }

    @Override
    public Type visit(final BooleanFalseLiteral node) {
        return new BooleanType(node.line, node.column);
    }

    @Override
    public Type visit(final ThisExpression node) {
        return new IdentifierType(currentClass.toString(), node.line, node.column);
    }

    @Override
    public Type visit(final NewArrayExpression node) {
        final Type t = node.size.accept(this);
        if (!t.toString().equals("INTEGER"))
            error.display(node.size.line, node.size.column, "Integer expression expected");

        return new IntArrayType(node.size.line, node.size.column);
    }

    @Override
    public Type visit(final NewObjectExpression node) {
        if (!symTable.typeExist(Symbol.symbol(node.id.n)))
            error.display(node.id.line, node.id.column, "undefined symbol '" + node.id.n + "'");

        return new IdentifierType(node.id.n, node.id.line, node.id.column);
    }

    @Override
    public Type visit(final NotExpression node) {
        final Type t = node.e.accept(this);
        if (!t.toString().equals("BOOLEAN"))
            error.display(node.e.line, node.e.column, "boolean expression expected");

        return new BooleanType(node.e.line, node.e.line);
    }

    @Override
    public Type visit(final ParenthesisExpression node) {
        return node.e.accept(this);
    }

    @Override
    public Type visit(final IdentifierExpression node) {
        final Type t = symTable.lookup(Symbol.symbol(node.n));
        if (t != null)
            return t;

        if (!symTable.typeExist(Symbol.symbol(node.n)))
            error.display(node.line, node.column, "undefined symbol '" + node.n + "'");

        return new IdentifierType(node.n, node.line, node.column);
    }

    @Override
    public Type visit(final Identifier node) {
        final Type t = symTable.lookup(Symbol.symbol(node.n));
        if (t != null)
            return t;

        if (!symTable.typeExist(Symbol.symbol(node.n)))
            error.display(node.line, node.column, "undefined symbol '" + node.n + "'");

        return new IdentifierType(node.n, node.line, node.column);
    }

}