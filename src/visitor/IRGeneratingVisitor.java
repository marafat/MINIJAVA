package visitor;

import ast.*;
import ast.expression.*;
import irt.*;
import symbol.ClassBinding;
import symbol.Symbol;
import symbol.TableIR;
import translate.*;
import temp.*;
import frame.*;

import java.util.*;

/**
 * @author marafat
 */

public class IRGeneratingVisitor extends AbstractVisitor<Expr> {

    private TableIR symTable;
    private String calledClass;
    private String currentClass;
    private String currentMethod;
    private int paramsCount;
    private String identifier;
    private List<Frag> frags;
    private Level topLevel;
    private Level level;
    private int wSize;
    public Label exit = null;

    public IRGeneratingVisitor(frame.Frame frame, final Map<Symbol, ClassBinding> forwardDecls) {
        level = topLevel = new Level(frame);
        symTable = new TableIR(forwardDecls);
        wSize = frame.wordSize();
        frags = new ArrayList<>();
    }

    public void traverse(final Node ast) {
        ast.accept(this);
    }

    private Level newLevel(Label name, boolean isLeaf, List<Boolean> formals) {
        formals.add(0, !isLeaf);
        return new Level(level, name, formals);
    }

    private void procEntryExit(Expr body) {
        Frame myframe = level.frame;
        Exp bodyExp = body.unEx();
        Stm bodyStm;
        if (bodyExp != null)
            bodyStm = new MOVE(new TEMP(myframe.RV()), bodyExp);
        else
            bodyStm = body.unNx();
        ProcFrag frag = new ProcFrag(bodyStm, myframe);
        frags.add(frag);
    }

    public List<Frag> getResult() {
        return frags;
    }


    @Override
    public Expr visit(Program node) {
        super.visit(node);
        DataFrag frag = new DataFrag(topLevel.frame.programTail());
        frags.add(frag);
        return null;
    }

    @Override
    public Expr visit(MainClass node) {
        currentClass = node.id.n;
        currentMethod = null;
        symTable.enterScope();
        node.mmd.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Expr visit(MainMethodDeclaration node) {
        Level oldLevel = level;
        currentMethod = "main";
        LinkedList<Boolean> params = new LinkedList<Boolean>();
        level = newLevel(new Label(currentClass+"."+currentMethod),false, params);
        symTable.enterScope();
        node.mmb.accept(this);
        symTable.exitScope();
        level = oldLevel;
        return null;
    }

    @Override
    public Expr visit(MainMethodBody node) {
        Expr e = node.s.accept(this);
        procEntryExit(e);
        return null;
    }

    @Override
    public Expr visit(ClassDeclarationSimple node) {
        currentClass = node.id.n;
        currentMethod = null;
        symTable.enterScope();
        symTable.createFieldList(Symbol.symbol(node.id.n));
        node.cb.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Expr visit(ClassDeclarationExtends node) {
        currentClass = node.ids.n;
        currentMethod = null;
        symTable.enterScope();
        symTable.createFieldList(Symbol.symbol(node.ids.n));
        node.cb.accept(this);
        symTable.exitScope();

        return null;
    }

    @Override
    public Expr visit(ClassBody node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(VarDeclaration node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(MethodDeclaration node) {
        currentMethod = node.id.n;
        symTable.enterScope();
        node.t.accept(this);
        if (node.pl != null)
            node.pl.accept(this);
        node.mb.accept(this);
        symTable.exitScope();
        return null;
    }

    @Override
    public Expr visit(ParameterList node) {
        paramsCount = node.idl.size();
        for (int i = 0; i < paramsCount; i++)
            symTable.insert(Symbol.symbol(node.idl.get(i).n), new Temp());

        return super.visit(node);
    }

    @Override
    public Expr visit(MethodBody node) {
        LinkedList<Boolean> params = new LinkedList<Boolean>();
        for (int i = 0; i <= paramsCount; i++)
            params.add(false);

        for (VarDeclaration v: node.vdl)
            symTable.insert(Symbol.symbol(v.id.n), new Temp());

        Level oldLevel = level;
        level = newLevel(new Label(currentClass+"."+currentMethod),false, params);

        Stm stms = null; //No statement
        if (node.sl.size() == 1) { //One statement
            Expr expr = node.sl.get(0).accept(this);
            stms = expr.unNx();
        }
        else if (node.sl.size() > 1) { //Multiple statements
            Expr expr;
            for (int i = 0; i < node.sl.size(); i++) {
                expr = node.sl.get(i).accept(this);
                if (i == 0)
                    stms = expr.unNx();
                else
                    stms = new SEQ(stms, expr.unNx());
            }
        }

        Expr re = node.re.accept(this);
        procEntryExit(new Ex(new ESEQ(stms, re.unEx())));
        level = oldLevel;

        return null;
    }

    @Override
    public Expr visit(IntArrayType node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(BooleanType node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(IntegerType node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(IdentifierType node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(Block node) {
        Stm stms = null; //No statement

        if (node.sl.size() == 1) //One statement
            return node.sl.get(0).accept(this);

        if (node.sl.size() > 1) {
            Expr expr;
            for (int i = 0; i < node.sl.size(); i++) { //Multiple statements
                expr = node.sl.get(i).accept(this);
                if (i == 0)
                    stms = expr.unNx();
                else
                    stms = new SEQ(stms, expr.unNx());
            }
        }

        return new Nx(stms);
    }

    @Override
    public Expr visit(IfStatement node) {
        Expr cond = node.e.accept(this);
        Expr stm1 = node.s1.accept(this);
        Expr stm2 = node.s2.accept(this);

        return new Nx(new IfThenElseExp(cond, stm1, stm2).unNx());
    }

    @Override
    public Expr visit(WhileStatement node) {
        Expr expr = node.e.accept(this);
        Expr body = node.s.accept(this);
        Label T = new Label();
        Label F = new Label();

        return new Nx(new SEQ(new SEQ(expr.unCx(T, F),
                                new SEQ(new LABEL(T),
                                        body.unNx())),
                       new LABEL(F)));
    }

    @Override
    public Expr visit(PrintStatement node) {
        Expr expr = node.e.accept(this);
        List<Exp> args = new LinkedList<>();
        args.add(expr.unEx());
        Stm p = new MOVE(new TEMP(new Temp()),
                new CALL(new NAME(new Label("_printint")), args));
        Expr q = new Nx(p);
        return q;
    }

    @Override
    public Expr visit(AssignStatement node) {
        Exp exp1 = node.id.accept(this).unEx();
        Exp exp2 = node.e.accept(this).unEx();

        if (exp1 instanceof TEMP){ //In reg
            return new Nx(new MOVE (exp1, exp2));
        }
        else { //locate the address and assign
            Temp t = new Temp(0);
            return new Nx(new MOVE(new MEM(new BINOP(Binop.PLUS, new TEMP(t), exp1)),
                                   exp2));
        }
    }

//    @Override
//    public Expr visit(ArrayAssignStatement node) {
//        Exp exp1 = node.id.accept(this).unEx();
//        if (!(exp1 instanceof TEMP)) { //if not in reg
//            Temp taux1 = new Temp();
//            Temp taux2 = new Temp();
//            //e1 = taux2 = t0 + exp1*4
//            exp1 = new ESEQ(new SEQ(new MOVE(new TEMP(taux1),
//                                           new BINOP(Binop.MUL, exp1, new CONST(wSize))),
//                                  new MOVE(new TEMP(taux2),
//                                           new MEM(new BINOP(Binop.PLUS,
//                                                   new TEMP(new Temp(0)),
//                                                   new TEMP(taux1))))),
//                            new TEMP(taux2));
//        }
//
//        Exp exp2 = node.e1.accept(this).unEx();
//        Temp t_index = new Temp();
//        Temp t_size = new Temp();
//        LinkedList<Exp> args1 = new LinkedList<Exp>();
//        Label T = new Label();
//        Label F = new Label();
//
//        exp2 = new ESEQ   //with bound checking
//                (new SEQ(new SEQ(new SEQ(new SEQ(new SEQ(new MOVE(new TEMP(t_index),
//                                                                   new BINOP(Binop.MUL, exp2, new CONST(wSize))),
//                                                          new MOVE(new TEMP(t_size),
//                                                                   new MEM(exp1))),
//                                                 new CJUMP(Relop.GE, new TEMP(t_index), new TEMP(t_size), T, F)),
//                                       new LABEL(T)),
//                               new MOVE(new TEMP(new Temp()),
//                                        new CALL(new NAME(new Label("_error")),args1))),
//                       new LABEL(F)),
//                 new TEMP(t_index));
//
//        Exp exp3 = node.e2.accept(this).unEx();
//        //e1+e2+4 = e3
//        return new Nx (new MOVE(new MEM(new BINOP(Binop.PLUS,
//                                                  exp1,
//                                                  new BINOP(Binop.PLUS, exp2, new CONST(wSize)))),
//                                exp3));
//    }

    @Override
    public Expr visit(ArrayAssignStatement node) {
        Exp exp1 = node.id.accept(this).unEx();
        if (!(exp1 instanceof TEMP)) {  //if not in reg
            Temp t = new Temp();
            exp1 = new ESEQ(new MOVE(new TEMP(t) ,new MEM(new BINOP(Binop.MUL, exp1, new CONST(wSize)))),
                            new TEMP(t));
        }

        Exp exp2 = node.e1.accept(this).unEx();
        Temp t_index = new Temp();
        Temp t_size = new Temp();
        LinkedList<Exp> args1 = new LinkedList<Exp>();
        Label T = new Label();
        Label F = new Label();

        exp2 = new ESEQ   //with bound checking
                (new SEQ(new SEQ(new SEQ(new SEQ(new SEQ(new MOVE(new TEMP(t_index), //Extra Instruction: not effiecient
                                                                  new BINOP(Binop.MUL, exp2, new CONST(wSize))),
                                                         new MOVE(new TEMP(t_size),
                                                                  new MEM(exp1))), //exp1:pointer to heap
                                                 new CJUMP(Relop.GE, new TEMP(t_index), new TEMP(t_size), T, F)),
                                         new LABEL(T)),
                                  new MOVE(new TEMP(new Temp()),
                                           new CALL(new NAME(new Label("_error")),args1))),
                          new LABEL(F)),
                 new TEMP(t_index));

        Exp exp3 = node.e2.accept(this).unEx();
        //Mem[e1+e2+4] = e3
        return new Nx (new MOVE(new MEM(new BINOP(Binop.PLUS,
                                                  exp1,
                                                  new BINOP(Binop.PLUS,
                                                            new BINOP(Binop.MUL,
                                                                      exp2,
                                                                      new CONST(wSize)),
                                                            new CONST(wSize)))),
                       exp3));
    }

    @Override
    public Expr visit(LogicalExpression node) {
        Temp t1 = new Temp();
        Label done = new Label();
        Label ok1 = new Label();
        Label ok2 = new Label();
        Exp left =  node.le.accept(this).unEx();
        Exp right = node.re.accept(this).unEx();

        return new Ex //This is for AND. Also add for OR
                (new ESEQ(
                        new SEQ(
                                new SEQ(
                                        new SEQ(
                                                new SEQ (
                                                        new SEQ (
                                                                new MOVE(new TEMP(t1),new CONST(0)),
                                                                        new CJUMP(Relop.EQ,
                                                                                  left,
                                                                                  new CONST(1), ok1, done)),
                                                        new SEQ(new LABEL(ok1),
                                                                new CJUMP(Relop.EQ,
                                                                right,
                                                                new CONST(1), ok2, done))),
                                                new SEQ(new LABEL(ok2),
                                                        new MOVE(new TEMP(t1),new CONST(1)))),
                                        new JUMP(done)),
                                new LABEL(done)),
                        new TEMP(t1)));
    }

    @Override
    public Expr visit(CompareExpression node) {
        Expr expl= node.le.accept(this);
        Expr expr= node.re.accept(this);
        Label T = new Label();
        Label F = new Label();
        Temp t = new Temp();
        switch (node.op) {
            case 0: //EQ
            case 1: //LT
            case 2: //LTE
            default:
                return new Ex(new ESEQ(new SEQ(new SEQ(new SEQ(new MOVE(new TEMP(t), new CONST(0)),
                                                               new CJUMP(Relop.LT, expl.unEx(), expr.unEx(), T, F)),
                                                        new SEQ(new LABEL(T),
                                                                new MOVE(new TEMP(t), new CONST(1)))),
                                               new LABEL(F)),
                                       new TEMP(t)));
        }
    }

    @Override
    public Expr visit(ArithmeticExpression node) {
        Binop op;
        switch (node.op) {
            case 0:
                op = Binop.PLUS;
                break;
            case 1:
                op = Binop.MINUS;
                break;
            case 2:
                op = Binop.MUL;
                break;
            default:
                op = null;
        }

        return new Ex(new BINOP(op, node.le.accept(this).unEx(), node.re.accept(this).unEx()));
    }

    @Override
    public Expr visit(ArrayLookupExpression node) {
        Temp t_index = new Temp();
        Temp t_size = new Temp();
        Exp e1 = node.ref.accept(this).unEx();
        Exp e2 = node.ss.accept(this).unEx();

        Label F = new Label();
        Label T = new Label();

        LinkedList<Exp> args1 = new LinkedList<Exp>();

        Stm s1 = new SEQ(new SEQ(new SEQ(new SEQ(new SEQ(new MOVE(new TEMP(t_index), //if in bytes multiply with 4
                                                                  new BINOP(Binop.MUL, e2, new CONST(wSize))),
                                                         new MOVE(new TEMP(t_size),new MEM(e1))),
                                                 new CJUMP(Relop.GE, new TEMP(t_index), new TEMP(t_size),T,F)),
                                         new LABEL(T)),
                                  new MOVE(new TEMP(new Temp()),
                                           new CALL(new NAME(new Label("_error")),args1))),
                          new LABEL(F));

        Temp t = new Temp();
        Stm s2 = new SEQ(s1,
                         new MOVE(new TEMP(t),
                                  new MEM(new BINOP(Binop.PLUS,
                                                    e1,
                                                    new BINOP(Binop.PLUS,
                                                              new BINOP(Binop.MUL,e2, new CONST(wSize)),
                                                              new CONST(wSize))))));

        return new Ex(new ESEQ(s2, new TEMP(t)));
    }

    @Override
    public Expr visit(ArrayLengthExpression node) {
        return super.visit(node);
    }

    @Override
    public Expr visit(CallExpression node) {
        Temp t1 = new Temp() ;
        Temp t2 = new Temp() ;
        Temp t3 = new Temp() ;
        int index = -1;
        Expr exp1 = node.ref.accept(this);  //this should return location of the first field for ref object
        //Expr m_loc = node.id.accept(this);  //Method exists and will be translated later or sooner
        String m_name = calledClass + node.id.n; //Method Label to avoid naming conflict
        //For fully qualified method name, determine the classname of the called method
        //Use exp1

        List<Exp> args = new LinkedList<>();
        args.add(0, exp1.unEx()); //Pass object as an extra argument to the method
        for(int i = 0; i < node.al.size(); i++) {
            args.add(node.al.get(i).accept(this).unEx());
        }

        return new Ex(new CALL(new NAME(new Label(m_name)), args));
    }

    @Override
    public Expr visit(NewArrayExpression node) {
        Temp t1 = new Temp();
        Temp t2 = new Temp();
        Label cj = new Label();
        Label F = new Label();
        Label T = new Label();

        Expr exp1 = node.size.accept(this);

        Exp size = new BINOP(Binop.MUL,
                             new BINOP(Binop.PLUS, exp1.unEx(), new CONST(1)),
                             new CONST(4));

        // 1. call _halloc get pointer to space allocated in t1
        List<Exp> args1 = new LinkedList<Exp>();
        args1.add(size);
        Stm s1 = new MOVE(new TEMP(t1),
                          new CALL(new NAME(new Label("_halloc")),args1));

        // 2.Initialization
        Stm s2 = new SEQ(
                        new SEQ(
                                new SEQ(
                                        new SEQ(
                                                new SEQ(
                                                        new SEQ(new MOVE(new TEMP(t2),new CONST(wSize)),
                                                                new SEQ (new LABEL(cj),
                                                                         new CJUMP(Relop.LT,
                                                                                   new TEMP(t2),
                                                                                   size,
                                                                                   F,T))),
                                                        new LABEL(T)),
                                                new MOVE(new MEM(new BINOP(Binop.PLUS,new TEMP(t1),new TEMP(t2))),
                                                         new CONST(0))),
                                        new MOVE(new TEMP(t2),
                                                 new BINOP(Binop.PLUS, new TEMP(t2), new CONST(wSize)))),
                                new JUMP(cj)),
                        new SEQ(new LABEL(F),
                                new MOVE(new MEM(new TEMP(t1)),
                                         new BINOP(Binop.MUL,exp1.unEx(),new CONST(4)))));

        return new Ex(new ESEQ(new SEQ(s1, s2), new TEMP(t1)));
    }

    @Override
    public Expr visit(NewObjectExpression node) {
        Temp t1 = new Temp();
        Temp t2 = new Temp();
        Temp t3 = new Temp();
        Label l1 = new Label();
        //Label l2 = new Label();
        Label T = new Label();
        Label F = new Label();

        Expr exp = node.id.accept(this);
        int sz_f = (symTable.GetAllocSizeF(calledClass) + 1)*4;

        Stm s4;
        if (sz_f > 4){
            s4 = (new SEQ
                    (new SEQ
                        (new SEQ
                                (new SEQ
                                        (new SEQ
                                                (new SEQ
                                                        (new SEQ
                                                                (new SEQ(new MOVE(new TEMP(t3),
                                                                                  new CONST(wSize)),
                                                                         new LABEL(l1)),
                                                                         new CJUMP(Relop.LT,
                                                                                   new TEMP(t3),
                                                                                   new CONST(sz_f),
                                                                                   T,F)),
                                                         new LABEL(T)),
                                                new MOVE(new MEM(new BINOP(Binop.PLUS, new TEMP(t2), new TEMP(t3))),
                                                         new CONST(0))),
                                        new MOVE(new TEMP(t3),
                                                 new BINOP(Binop.PLUS, new TEMP(t3), new CONST(wSize)))),
                                new JUMP(l1)),
                        new LABEL(F)),
                    new MOVE(new MEM(new TEMP(t2)),new TEMP(t1))));
        }
        else{
            s4= (new MOVE(new MEM(new TEMP(t2)), new TEMP(t1)));
        }

        LinkedList<Exp> args2 = new LinkedList<Exp>();
        args2.add(new CONST(sz_f));

        Stm s2 = new MOVE(new TEMP(t2), new CALL(new NAME(new Label("_halloc")), args2)); //allocate
        //field space for this object and return the starting address in t2

        return new Ex(new ESEQ(new SEQ(s2, s4),new TEMP(t2)));
    }

    @Override
    public Expr visit(NotExpression node) {
        return new Ex(new BINOP(Binop.MINUS, new CONST(1),
                        (node.e.accept(this)).unEx()));
    }

    @Override
    public Expr visit(ParenthesisExpression node) {
        return node.e.accept(this);
    }

    @Override
    public Expr visit(IntegerLiteral node) {
        return new Ex(new CONST(node.value));
    }

    @Override
    public Expr visit(BooleanTrueLiteral node) {
        return new Ex(new CONST(1));
    }

    @Override
    public Expr visit(BooleanFalseLiteral node) {
        return new Ex(new CONST(0));
    }

    @Override
    public Expr visit(ThisExpression node) {
        calledClass = currentClass;
        return new Ex(new TEMP(new Temp(0)));
    }

    @Override
    public Expr visit(IdentifierExpression node) {
        calledClass = node.n;
        Temp t = symTable.lookupLocal(Symbol.symbol(node.n));
        if (t != null)
            return new Ex(new TEMP(t));

        int offset = symTable.lookupGlobal(Symbol.symbol(node.n));
        if (offset != -1) {
            Temp temp = new Temp();
            return new Ex(new MEM(new BINOP(Binop.PLUS, new TEMP(temp), new CONST(offset))));
        }

        return null;
    }

    @Override
    public Expr visit(Identifier node) {
        calledClass = node.n;
        Temp t = symTable.lookupLocal(Symbol.symbol(node.n));
        if (t != null)
            return new Ex(new TEMP(t));

        int offset = symTable.lookupGlobal(Symbol.symbol(node.n));
        if (offset != -1) {
            Temp temp = new Temp();
            return new Ex(new MEM(new BINOP(Binop.PLUS, new TEMP(temp), new CONST(offset))));
        }

        return null;
    }
}
