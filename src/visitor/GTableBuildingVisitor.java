package visitor;

import ast.*;
import ast.expression.*;
import error.Error;
import symbol.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author marafat
 */

public class GTableBuildingVisitor extends AbstractVisitor<Void> {

    private Error error;
    private Symbol currentClass;
    private Map<Symbol, ClassBinding> globalTable;

    public GTableBuildingVisitor() {
        globalTable = new HashMap<>();
        error = new Error();
    }

    public Map<Symbol, ClassBinding> traverse(final Node ast) {
        ast.accept(this);
        if (error.getCount() > 0)
            System.exit(1);
        return globalTable;
    }

    @Override
    public Void visit(final Program node) {
        return super.visit(node);
    }

    @Override
    public Void visit(final MainClass node) {
        currentClass = Symbol.symbol(node.id.n);
        globalTable.put(Symbol.symbol(node.id.n), new ClassBinding(null));
        return null;
    }

    @Override
    public Void visit(final MainMethodDeclaration node) {
        globalTable.get(currentClass).addMethod(new MethodBinding(Symbol.symbol("main"), null, new ArrayList<Type>()));
        return null;
    }

    @Override
    public Void visit(final MainMethodBody node) {
        return null;
    }

    @Override
    public Void visit(final ClassDeclarationSimple node) {
        currentClass = Symbol.symbol(node.id.n);
        final ClassBinding status = globalTable.put(Symbol.symbol(node.id.n), new ClassBinding(null));
        if (status != null)
            error.display(node.id.line, node.id.column, "class '" + node.id.n + "' already exists");

        node.cb.accept(this);

        return null;
    }

    @Override
    public Void visit(final ClassDeclarationExtends node) {
        currentClass = Symbol.symbol(node.ids.n);
        final ClassBinding status = globalTable.put(Symbol.symbol(node.ids.n), new ClassBinding(Symbol.symbol(node.idx.n)));
        if (status != null)
            error.display(node.ids.line, node.ids.column, "class '" + node.ids.n + "' already exists");

        node.cb.accept(this);

        return null;
    }

    @Override
    public Void visit(final ClassBody node) {
        for (final VarDeclaration v: node.vd) {
            boolean status = globalTable.get(currentClass).addField(new VarBinding(Symbol.symbol(v.id.n), v.t, 1));
            if (!status)
                error.display(v.id.line, v.id.column, "field '" + v.id.n + "' already exists");
        }

        for (final MethodDeclaration m: node.md)
            if (m != null)
                m.accept(this);

        return null;
    }

    @Override
    public Void visit(final VarDeclaration node) {
        return null;
    }

    @Override
    public Void visit(MethodDeclaration node) {
        boolean status;
        if (node.pl == null)
            status = globalTable.get(currentClass).addMethod(new MethodBinding(Symbol.symbol(node.id.n), node.t, new ArrayList<Type>()));
        else
            status = globalTable.get(currentClass).addMethod(new MethodBinding(Symbol.symbol(node.id.n), node.t, node.pl.tl));

        if (!status)
            error.display(node.id.line, node.id.column, "method '" + node.id.n + "' already exists");

        return null;
    }

    @Override
    public Void visit(final ParameterList node) {
        return null;
    }

    @Override
    public Void visit(final MethodBody node) {
        return null;
    }

    @Override
    public Void visit(final IntArrayType node) {
        return null;
    }

    @Override
    public Void visit(final BooleanType node) {
        return null;
    }

    @Override
    public Void visit(final IntegerType node) {
        return null;
    }

    @Override
    public Void visit(final IdentifierType node) {
        return null;
    }

    @Override
    public Void visit(final Block node) {
        return null;
    }

    @Override
    public Void visit(final IfStatement node) {
        return null;
    }

    @Override
    public Void visit(final WhileStatement node) {
        return null;
    }

    @Override
    public Void visit(final PrintStatement node) {
        return null;
    }

    @Override
    public Void visit(final AssignStatement node) {
        return null;
    }

    @Override
    public Void visit(final ArrayAssignStatement node) {
        return null;
    }

    @Override
    public Void visit(final LogicalExpression node) {
        return null;
    }

    @Override
    public Void visit(final CompareExpression node) {
        return null;
    }

    @Override
    public Void visit(final ArithmeticExpression node) {
        return null;
    }

    @Override
    public Void visit(final ArrayLookupExpression node) {
        return null;
    }

    @Override
    public Void visit(final ArrayLengthExpression node) {
        return null;
    }

    @Override
    public Void visit(final CallExpression node) {
        return null;
    }

    @Override
    public Void visit(final NewArrayExpression node) {
        return null;
    }

    @Override
    public Void visit(final NewObjectExpression node) {
        return null;
    }

    @Override
    public Void visit(final NotExpression node) {
        return null;
    }

    @Override
    public Void visit(final ParenthesisExpression node) {
        return null;
    }

    @Override
    public Void visit(final IntegerLiteral node) {
        return null;
    }

    @Override
    public Void visit(final BooleanTrueLiteral node) {
        return null;
    }

    @Override
    public Void visit(final BooleanFalseLiteral node) {
        return null;
    }

    @Override
    public Void visit(final ThisExpression node) {
        return null;
    }

    @Override
    public Void visit(final IdentifierExpression node) {
        return null;
    }

    @Override
    public Void visit(final Identifier node) {
        return null;
    }

}
