package visitor;

import ast.*;
import ast.expression.*;
import frontend.MiniJavaBaseVisitor;
import frontend.MiniJavaParser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author marafat
 */

public class ASTBuildingVisitor extends MiniJavaBaseVisitor<Node> {

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitProgram(MiniJavaParser.ProgramContext ctx) {
        final List<ClassDeclaration> cdl = new ArrayList<>();
        final MainClass mc = (MainClass) visit(ctx.mainClass());

        for (final MiniJavaParser.ClassDeclarationContext cdc: ctx.classDeclaration())
            if (cdc != null)
                cdl.add((ClassDeclaration) visit(cdc));

        return new Program(mc, cdl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMainClass(MiniJavaParser.MainClassContext ctx) {
        final Identifier id = (Identifier) visit(ctx.identifier());
        final MainMethodDeclaration mmd = (MainMethodDeclaration) visit(ctx.mainMethodDeclaration());
        return new MainClass(id, mmd, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMainMethodDeclaration(MiniJavaParser.MainMethodDeclarationContext ctx) {
        final Identifier id = (Identifier) visit(ctx.identifier());
        final MainMethodBody mmb = (MainMethodBody) visit(ctx.mainMethodBody());
        return new MainMethodDeclaration(id, mmb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMainMethodBody(MiniJavaParser.MainMethodBodyContext ctx) {
        final Statement s = (Statement) visit(ctx.statement());
        return new MainMethodBody(s, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassDeclaration(MiniJavaParser.ClassDeclarationContext ctx) {
        return super.visitClassDeclaration(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassDeclarationSimple(MiniJavaParser.ClassDeclarationSimpleContext ctx) {
        final Identifier id = (Identifier) visit(ctx.identifier());
        final ClassBody cb = (ClassBody) visit(ctx.classBody());
        return new ClassDeclarationSimple(id, cb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassDeclarationExtends(MiniJavaParser.ClassDeclarationExtendsContext ctx) {
        final Identifier simple = (Identifier) visit(ctx.identifier(0));
        final Identifier extend = (Identifier) visit(ctx.identifier(1));
        final ClassBody cb = (ClassBody) visit(ctx.classBody());
        return new ClassDeclarationExtends(simple, extend, cb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitClassBody(MiniJavaParser.ClassBodyContext ctx) {
        final List<VarDeclaration> vdl = new ArrayList<>();
        for (final MiniJavaParser.VarDeclarationContext v: ctx.varDeclaration())
            if (v != null)
                vdl.add((VarDeclaration) visit(v));

        final List<MethodDeclaration> mdl = new ArrayList<>();
        for (final MiniJavaParser.MethodDeclarationContext m : ctx.methodDeclaration())
            if (m != null)
                mdl.add((MethodDeclaration) visit(m));

        return new ClassBody(vdl, mdl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitVarDeclaration(MiniJavaParser.VarDeclarationContext ctx) {
        final Type t = (Type) visit(ctx.type());
        final Identifier id = (Identifier) visit(ctx.identifier());
        return new VarDeclaration(t, id, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMethodDeclaration(MiniJavaParser.MethodDeclarationContext ctx) {
        final Type t = (Type) visit(ctx.type());
        final Identifier id = (Identifier) visit(ctx.identifier());
        ParameterList pl = null;
        if (ctx.parameterList() != null)
            pl = (ParameterList) visit(ctx.parameterList());
        final MethodBody mb = (MethodBody) visit(ctx.methodBody());
        return new MethodDeclaration(t, id, pl, mb, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitParameterList(MiniJavaParser.ParameterListContext ctx) {
        final List<Type> tl = new ArrayList<>();
        for(final MiniJavaParser.TypeContext t: ctx.type())
            if (t != null)
                tl.add((Type) visit(t));

        final List<Identifier> idl = new ArrayList<>();
        for (final MiniJavaParser.IdentifierContext id: ctx.identifier())
            if (id != null)
                idl.add((Identifier) visit(id));

        return new ParameterList(tl, idl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitMethodBody(MiniJavaParser.MethodBodyContext ctx) {
        final List<VarDeclaration> vdl = new ArrayList<>();
        for (final MiniJavaParser.VarDeclarationContext vd: ctx.varDeclaration())
            if (vd != null)
                vdl.add((VarDeclaration) visit(vd));

        final List<Statement> sl = new ArrayList<>();
        for (final MiniJavaParser.StatementContext s: ctx.statement())
            if (s != null)
                sl.add((Statement) visit(s));

        final Expression e = (Expression) visit(ctx.expression());

        return new MethodBody(vdl, sl, e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitType(MiniJavaParser.TypeContext ctx) {
        return super.visitType(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIntArrayType(MiniJavaParser.IntArrayTypeContext ctx) {
        return new IntArrayType(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBooleanType(MiniJavaParser.BooleanTypeContext ctx) {
        return new BooleanType(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIntegerType(MiniJavaParser.IntegerTypeContext ctx) {
        return new IntegerType(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIdentifierType(MiniJavaParser.IdentifierTypeContext ctx) {
        return new IdentifierType(ctx.ID().getText(), ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitStatement(MiniJavaParser.StatementContext ctx) {
        return super.visitStatement(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBlock(MiniJavaParser.BlockContext ctx) {
        final List<Statement> sl = new ArrayList<>();
        for (final MiniJavaParser.StatementContext s: ctx.statement())
            if (s != null)
                sl.add((Statement) visit(s));

        return new Block(sl, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIfStatement(MiniJavaParser.IfStatementContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        final Statement s1 = (Statement) visit(ctx.statement(0));
        final Statement s2 = (Statement) visit(ctx.statement(1));
        return new IfStatement(e, s1, s2, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitWhileStatement(MiniJavaParser.WhileStatementContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        final Statement s1 = (Statement) visit(ctx.statement());
        return new WhileStatement(e, s1, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitPrintStatement(MiniJavaParser.PrintStatementContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        return new PrintStatement(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitAssignStatement(MiniJavaParser.AssignStatementContext ctx) {
        final Identifier id = (Identifier) visit(ctx.identifier());
        final Expression e = (Expression) visit(ctx.expression());
        return new AssignStatement(id, e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArrayAssignStatement(MiniJavaParser.ArrayAssignStatementContext ctx) {
        final Identifier id = (Identifier) visit(ctx.identifier());
        final Expression e1 = (Expression) visit(ctx.expression(0));
        final Expression e2 = (Expression) visit(ctx.expression(1));
        return new ArrayAssignStatement(id, e1, e2, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitParenthesisExpression(MiniJavaParser.ParenthesisExpressionContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        return new ParenthesisExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArrayLookupExpression(MiniJavaParser.ArrayLookupExpressionContext ctx) {
        final Expression e1 = (Expression) visit(ctx.expression(0));
        final Expression e2 = (Expression) visit(ctx.expression(1));
        return new ArrayLookupExpression(e1, e2, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitNotExpression(MiniJavaParser.NotExpressionContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        return new NotExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitCompareExpression(MiniJavaParser.CompareExpressionContext ctx) {
        final Expression l = (Expression) visit(ctx.expression(0));
        int op = ctx.op.getType();
        final Expression r = (Expression) visit(ctx.expression(1));
        return new CompareExpression(l, op, r, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArithmeticExpression(MiniJavaParser.ArithmeticExpressionContext ctx) {
        final Expression l = (Expression) visit(ctx.expression(0));
        int op = ctx.op.getType();
        final Expression r = (Expression) visit(ctx.expression(1));
        return new ArithmeticExpression(l, op, r, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitNewObjectExpression(MiniJavaParser.NewObjectExpressionContext ctx) {
        final Identifier id = (Identifier) visit(ctx.identifier());
        return new NewObjectExpression(id, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIdentifierExpression(MiniJavaParser.IdentifierExpressionContext ctx) {
        return new IdentifierExpression(ctx.ID().getText(), ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitThisExpression(MiniJavaParser.ThisExpressionContext ctx) {
        return new ThisExpression(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitLogicalExpression(MiniJavaParser.LogicalExpressionContext ctx) {
        final Expression l = (Expression) visit(ctx.expression(0));
        int op = ctx.op.getType();
        final Expression r = (Expression) visit(ctx.expression(1));
        return new LogicalExpression(l, op, r, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitArrayLengthExpression(MiniJavaParser.ArrayLengthExpressionContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        return new ArrayLengthExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBooleanTrueLiteral(MiniJavaParser.BooleanTrueLiteralContext ctx) {
        return new BooleanTrueLiteral(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitNewArrayExpression(MiniJavaParser.NewArrayExpressionContext ctx) {
        final Expression e = (Expression) visit(ctx.expression());
        return new NewArrayExpression(e, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitCallExpression(MiniJavaParser.CallExpressionContext ctx) {
        final Expression e = (Expression) visit(ctx.expression(0));
        final Identifier id = (Identifier) visit(ctx.identifier());

        List<Expression> al = new ArrayList<>();
        for (int i = 1; i < ctx.expression().size(); i++) {
            al.add((Expression) visit(ctx.expression(i)));
        }

        return new CallExpression(e, id, al, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIntegerLiteral(MiniJavaParser.IntegerLiteralContext ctx) {
        final int val = Integer.parseInt(ctx.INTEGER_LITERAL().getText());
        return new IntegerLiteral(val, ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitBooleanFalseLiteral(MiniJavaParser.BooleanFalseLiteralContext ctx) {
        return new BooleanFalseLiteral(ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     *
     * @param ctx
     */
    @Override
    public Node visitIdentifier(MiniJavaParser.IdentifierContext ctx) {
        return new Identifier(ctx.ID().getText(), ctx.start.getLine(), ctx.start.getCharPositionInLine());
    }
}
