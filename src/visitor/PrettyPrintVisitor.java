package visitor;

import ast.*;
import ast.expression.*;

/**
 * @author marafat
 */

public class PrettyPrintVisitor extends AbstractVisitor<Void> {

    private int indent = 0;

    private void enterNode(final String nodeLabel) {
        for (int i = 0; i < indent; i++)
            System.out.print("    ");
        System.out.println(nodeLabel);
        indent++;
    }

    private Void exitNode() {
        indent--;
        return null;
    }

    @Override
    public Void visit(Program node) {
        enterNode("Program");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(MainClass node) {
        enterNode("MainClass");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(MainMethodDeclaration node) {
        enterNode("MainMethodDeclaration");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(MainMethodBody node) {
        enterNode("MainMethodBody");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ClassDeclarationSimple node) {
        enterNode("ClassDeclarationSimple");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ClassDeclarationExtends node) {
        enterNode("ClassDeclarationExtends");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ClassBody node) {
        enterNode("ClassBody");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(VarDeclaration node) {
        enterNode("VarDeclaration");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(MethodDeclaration node) {
        enterNode("MethodDeclaration");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ParameterList node) {
        enterNode("ParameterList");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(MethodBody node) {
        enterNode("MethodBody");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(IntArrayType node) {
        enterNode("IntArrayType");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(BooleanType node) {
        enterNode("Boolean Type");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(IntegerType node) {
        enterNode("IntegerType");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(IdentifierType node) {
        enterNode("IdentifierType");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(Block node) {
        enterNode("Block");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(IfStatement node) {
        enterNode("IfStatement");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(WhileStatement node) {
        enterNode("WhileStatement");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(PrintStatement node) {
        enterNode("PrintStatement");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(AssignStatement node) {
        enterNode("AssignStatement");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ArrayAssignStatement node) {
        enterNode("ArrayAssignStatement");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(LogicalExpression node) {
        enterNode("LogicalExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(CompareExpression node) {
        enterNode("CompareExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ArithmeticExpression node) {
        enterNode("ArithmeticExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ArrayLookupExpression node) {
        enterNode("ArrayLookupExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ArrayLengthExpression node) {
        enterNode("ArrayLengthExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(CallExpression node) {
        enterNode("CallExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(NewArrayExpression node) {
        enterNode("NewArrayExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(NewObjectExpression node) {
        enterNode("NewObjectExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(NotExpression node) {
        enterNode("NotExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ParenthesisExpression node) {
        enterNode("ParenthesisExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(IntegerLiteral node) {
        enterNode("IntegerLiteral");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(BooleanTrueLiteral node) {
        enterNode("BooleanTrueLiteral");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(BooleanFalseLiteral node) {
        enterNode("BooleanTrueLiteral");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(ThisExpression node) {
        enterNode("ThisExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(IdentifierExpression node) {
        enterNode("IdentifierExpression");
        super.visit(node);
        return exitNode();
    }

    @Override
    public Void visit(Identifier node) {
        enterNode("Identifier");
        super.visit(node);
        return exitNode();
    }
}
