package frame;

import irt.*;
/**
 * @author marafat
 */

public abstract class Access {

    public abstract Exp exp(Exp framePtr);
    public abstract String toString();
}
