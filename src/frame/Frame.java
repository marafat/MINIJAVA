package frame;

import temp.Label;
import temp.Temp;

import java.util.List;

/**
 * @author marafat
 */

public abstract class Frame implements temp.TempMap{

    public Label name;
    public List<Access> formals;

    public abstract Frame newFrame(Label name, List<Boolean> formals);
    public abstract Access allocLocal(boolean escape);
    public abstract temp.Temp FP();
    public abstract int wordSize();
    public abstract irt.Exp externalCall(String func, List<irt.Exp> args);
    public abstract temp.Temp RV();
    public abstract String tempMap(Temp temp);
    public abstract List<Temp[]> registers();
    public abstract String programTail();
}
