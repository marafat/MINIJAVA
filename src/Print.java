//package irt;

import temp.DefaultMap;
import temp.TempMap;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

import irt.*;

/**
 * @author marafat
 */

public class Print {

    PrintWriter out;
    TempMap tmap;

    public Print(PrintWriter out, TempMap tmap, Stm s) {
        this.out = out;
        this.tmap = tmap;
        prStm(s, 0);
    }

    public Print(PrintStream o) {
        this.out = out;
        this.tmap = new DefaultMap();
    }

    private void indent(int d) {
        for(int i=0; i<d; i++)
            out.print(' ');
    }

    private void say(String s) {
        out.print(s);
    }

    void sayln(String s) {
        say(s); say("\n");
    }

    void prStm(SEQ s, int d) {
        indent(d); sayln("SEQ("); prStm(s.left,d+1); sayln(",");
        prStm(s.right,d+1); say(")");
    }

    void prStm(LABEL s, int d) {
        indent(d); say("LABEL "); say(s.label.toString());
    }

    void prStm(JUMP s, int d) {
        indent(d); sayln("JUMP("); prExp(s.exp, d+1); say(")");
    }

    void prStm(CJUMP s, int d) {
        indent(d); say("CJUMP(");
        switch(s.op) {
            case EQ: say("EQ"); break;
            case NE: say("NE"); break;
            case LT: say("LT"); break;
            case GT: say("GT"); break;
            case LE: say("LE"); break;
            case GE: say("GE"); break;
            case ULT: say("ULT"); break;
            case ULE: say("ULE"); break;
            case UGT: say("UGT"); break;
            case UGE: say("UGE"); break;
            default:
                throw new Error("Print.prStm.CJUMP");
        }
        sayln(","); prExp(s.left,d+1); sayln(",");
        prExp(s.right,d+1); sayln(",");
        indent(d+1); say(s.iftrue.toString()); say(",");
        say(s.iffalse.toString()); say(")");
    }

    void prStm(MOVE s, int d) {
        indent(d); sayln("MOVE("); prExp(s.dst,d+1); sayln(",");
        prExp(s.src,d+1); say(")");
    }

    void prStm(EXP s, int d) {
        indent(d); sayln("EXP("); prExp(s.exp,d+1); say(")");
    }

    void prStm(Stm s, int d) {
        if (s instanceof SEQ) prStm((SEQ)s, d);
        else if (s instanceof LABEL) prStm((LABEL)s, d);
        else if (s instanceof JUMP) prStm((JUMP)s, d);
        else if (s instanceof CJUMP) prStm((CJUMP)s, d);
        else if (s instanceof MOVE) prStm((MOVE)s, d);
        else if (s instanceof EXP) prStm((EXP)s, d);
        else throw new Error("Print.prStm");
    }

    void prExp(BINOP e, int d) {
        indent(d); say("BINOP(");
        switch(e.op) {
            case PLUS: say("PLUS"); break;
            case MINUS: say("MINUS"); break;
            case MUL: say("MUL"); break;
            case DIV: say("DIV"); break;
            case AND: say("AND"); break;
            case OR: say("OR"); break;
            case LSHIFT: say("LSHIFT"); break;
            case RSHIFT: say("RSHIFT"); break;
            case ARSHIFT: say("ARSHIFT"); break;
            case XOR: say("XOR"); break;
            default:
                throw new Error("Print.prExp.BINOP");
        }
        sayln(",");
        prExp(e.left,d+1); sayln(","); prExp(e.right,d+1); say(")");
    }

    void prExp(MEM e, int d) {
        indent(d);
        sayln("MEM("); prExp(e.exp,d+1); say(")");
    }

    void prExp(TEMP e, int d) {
        indent(d); say("TEMP ");
        say(tmap.tempMap(e.temp));
    }

    void prExp(ESEQ e, int d) {
        indent(d); sayln("ESEQ(");
        prStm(e.stm,d+1);
        sayln(",");
        prExp(e.exp,d+1);
        say(")");

    }

    void prExp(NAME e, int d) {
        indent(d); say("NAME "); say(e.label.toString());
    }

    void prExp(CONST e, int d) {
        indent(d); say("CONST "); say(String.valueOf(e.value));
    }

    void prExp(CALL e, int d) {
        indent(d); sayln("CALL(");
        prExp(e.func,d+1);
        for(Exp a: e.args) {
            sayln(","); prExp(a,d+2);
        }
        say(")");
    }

    void prExp(Exp e, int d) {
        if (e instanceof BINOP) prExp((BINOP)e, d);
        else if (e instanceof MEM) prExp((MEM)e, d);
        else if (e instanceof TEMP) prExp((TEMP)e, d);
        else if (e instanceof ESEQ) prExp((ESEQ)e, d);
        else if (e instanceof NAME) prExp((NAME)e, d);
        else if (e instanceof CONST) prExp((CONST)e, d);
        else if (e instanceof CALL) prExp((CALL)e, d);
        else throw new Error("Print.prExp");
    }

    public void prStm(Stm s) {prStm(s,0); say("\n");}

    public void prExp(Exp e) {prExp(e,0); say("\n");}

}
