package symbol;

import ast.*;
import temp.Temp;

import java.util.*;

/**
 * @author marafat
 */

public class TableIR {

    private Deque<VarBinding> local;
    private Set<VarBinding> global;
    private Map<Symbol, ClassBinding> forwardDecls;
    private int scope;

    public TableIR(final Map<Symbol, ClassBinding> forwardDecls) {
        local = new ArrayDeque<>();
        global = new LinkedHashSet<>();
        this.forwardDecls = forwardDecls;
        scope = 0;
    }

    public void enterScope() {
        scope++;
    }

    public void exitScope() {
        for (final VarBinding v: local) {
            if (v.getScope() < scope)
                break;
            local.pop();
        }

        scope--;
    }

    public void insert(final Symbol key, final Temp t) {
        final VarBinding v = new VarBinding(key, t, scope);
        local.addFirst(v);
    }

    public void createFieldList(final Symbol parentClass) {
        global.clear();
        Symbol currentClass = parentClass;
        while (currentClass != null) {
            global.addAll(forwardDecls.get(currentClass).getFields());
            currentClass = forwardDecls.get(parentClass).getSuperClass();
        }
    }

    public Temp lookupLocal(final Symbol key) {
        for (final VarBinding v: local) {
            if (v.getKey().equals(key))
                return v.getTemp();
        }

        return null;
    }

    public int lookupGlobal(final Symbol key) {
        int i = 1;
        for (VarBinding v: global) {
            if (v.getKey().equals(key))
                return i;
        }

        return -1;
    }

    public boolean typeExist(final Symbol name) {
        return forwardDecls.containsKey(name);
    }

    public Type getReturnType(final Symbol cls, final Symbol method, final List<Type> params) {
        if (typeExist(cls)) {
            return forwardDecls.get(cls).getReturnType(method, params);
        }

        return null;
    }

    public boolean isSuperClass(final Symbol superClass, final Symbol subClass) {
        if (superClass.toString().equals("BOOLEAN") || superClass.toString().equals("INTEGER") ||
                subClass.toString().equals("BOOLEAN") || subClass.toString().equals("INTEGER"))
            return false;

        Symbol parentClass = subClass;
        while (parentClass != null) {
            if (superClass.equals(parentClass))
                return true;
            parentClass = forwardDecls.get(parentClass).getSuperClass();
        }

        return false;
    }

    public int GetAllocSizeF(String classname) {
        return 1;
    }

}