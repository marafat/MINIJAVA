package symbol;

import ast.MethodDeclaration;
import ast.Type;
import temp.Label;

import java.util.ArrayList;
import java.util.List;

/**
 * @author marafat
 */

public class MethodBinding extends Binding{

    private Symbol key;
    private Type rType;
    private List<Type> parameters;
    private Label lab;

    public MethodBinding(final Symbol key, final Type rType, final List<Type> parameters) {
        this.key = key;
        this.rType = rType;
        this.parameters = parameters;
        this.lab = generateLabel();
    }

    public Symbol getKey() {
        return key;
    }

    public Type getrType() {
        return rType;
    }

    public List<Type> getParameters() {
        return parameters;
    }

    public Label getLabel() {
        return lab;
    }

    private Label generateLabel() {
        String fqName = key.toString();
        for(Type t: parameters) {
            switch (t.toString()) {
                case "INT":
                    fqName = fqName + "I";
                    break;
                case "BOOLEAN":
                    fqName = fqName + "B";
                    break;
                case "INTARRAY":
                    fqName = fqName + "I[";
                    break;
                case "IDENTIFIER":
                    fqName = fqName + "ID";
                    break;
                default:
                    break;
            }
        }
        return new Label(fqName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MethodBinding that = (MethodBinding) o;

        if (!key.equals(that.key)) return false;
        for (int i = 0; i < parameters.size(); i++) {
            if (!that.parameters.get(0).getClass().equals(this.parameters.get(0).getClass()))
                return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        if (parameters != null) {
            result = 31 * result;
            for (Type t : parameters)
                result = result + t.getClass().hashCode();
            return result;
        }
        //result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        return 31 * result;
    }
}
