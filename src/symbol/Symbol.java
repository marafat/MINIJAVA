package symbol;

import java.util.HashMap;
import java.util.Map;

public class Symbol {

    private String name;
    private static Map<String, Symbol> dictionary = new HashMap<>();

    private Symbol(final String name) {this.name = name;}

    public static Symbol symbol(final String n) { //Symbol factory
        String u = n.intern();
        Symbol s = dictionary.get(u);
        if (s == null) {
            s = new Symbol(u);
            dictionary.put(u,s);
        }
        return s;
    }

    public String toString() {return name;}
}
