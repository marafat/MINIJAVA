package symbol;

import ast.Type;
import temp.Temp;

/**
 * @author marafat
 */

public class VarBinding extends Binding {

    private Symbol key;
    private int scope;
    private Type type;
    private Temp temp;

    public VarBinding(final Symbol key, final Type type, final int scope) {
        this.key = key;
        this.type = type;
        this.scope = scope;
    }

    public VarBinding(final Symbol key, final Temp temp, final int scope) {
        this.key = key;
        this.scope = scope;
        this.temp = temp;
    }

    public Symbol getKey() {
        return key;
    }

    public Type getType() {
        return type;
    }

    public int getScope() {
        return scope;
    }

    public Temp getTemp() {
        return temp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VarBinding that = (VarBinding) o;

        if (scope != that.scope) return false;
        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + scope;
        return result;
    }
}
