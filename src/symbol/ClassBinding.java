package symbol;

import ast.Type;

import java.util.*;

/**
 * @author marafat
 */

public class ClassBinding extends Binding {

    private Symbol superClass;
    private Set<VarBinding> fields;
    private Set<MethodBinding> methods;

    public ClassBinding(final Symbol superClass) {
        fields = new LinkedHashSet<>();
        methods = new LinkedHashSet<>();
        this.superClass = superClass;
    }

    public boolean addField(final VarBinding v) {
        return fields.add(v);
    }

    public boolean addMethod(final MethodBinding mb) {
        return methods.add(mb);
    }

    public Symbol getSuperClass() {
        return superClass;
    }

    public Set<VarBinding> getFields() {
        return fields;
    }

    public Type getReturnType(final Symbol key, final List<Type> params) {
        for (MethodBinding this_m: methods)
            if (this_m.getKey().equals(key)) {
                if (this_m.getParameters().size() == params.size()) {
                    for (int i = 0; i < params.size(); i++)
                        if (!this_m.getParameters().get(i).equals(params.get(i)))
                            break;
                    return this_m.getrType();
                }
            }

        return null;
    }

}
