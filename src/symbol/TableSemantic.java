package symbol;

import ast.*;
import java.util.*;

/**
 * @author marafat
 */

public class TableSemantic {

    private Deque<VarBinding> stack;
    private Map<Symbol, ClassBinding> forwardDecls;
    private int scope;

    public TableSemantic(final Map<Symbol, ClassBinding> forwardDecls) {
        stack = new ArrayDeque<>();
        this.forwardDecls = forwardDecls;
        scope = 0;
    }

    public void enterScope() {
        scope++;
    }

    public void exitScope() {
        for (final VarBinding v: stack) {
            if (v.getScope() < scope)
                break;
            stack.pop();
        }

        scope--;
    }

    public boolean insert(final Symbol key, Type type) {
        final VarBinding v = new VarBinding(key, type, scope);
        if (stack.contains(v))
            return false;
        stack.addFirst(v);
        return true;
    }

    public void insertField(final Symbol key, final Type type) {
        stack.addFirst(new VarBinding(key, type, scope));
    }

    public boolean insertSuperclassField(Symbol key) {
        if (!typeExist(key))
            return false;

        for (final VarBinding v: forwardDecls.get(key).getFields())
            stack.addFirst(v);

        return true;
    }

    public Type lookup(final Symbol key) {
        for (final VarBinding v: stack) {
            if (v.getKey().equals(key))
                return v.getType();
        }

        return null;
    }

    public boolean typeExist(final Symbol name) {
        return forwardDecls.containsKey(name);
    }

    public Type getReturnType(final Symbol cls, final Symbol method, final List<Type> params) {
        if (typeExist(cls)) {
            return forwardDecls.get(cls).getReturnType(method, params);
        }

        return null;
    }

    public boolean isSuperClass(final Symbol superClass, final Symbol subClass) {
        if (superClass.toString().equals("BOOLEAN") || superClass.toString().equals("INTEGER") ||
                subClass.toString().equals("BOOLEAN") || subClass.toString().equals("INTEGER"))
            return false;

        Symbol parentClass = subClass;
        while (parentClass != null) {
            if (superClass.equals(parentClass))
                return true;
            parentClass = forwardDecls.get(parentClass).getSuperClass();
        }

        return false;
    }

}

