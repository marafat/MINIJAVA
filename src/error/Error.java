package error;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * @author marafat
 */

public class Error extends BaseErrorListener {

    private int errorCount;

    public Error() {
        errorCount = 0;
    }

    public int getCount() {
        return errorCount;
    }

    public void display(int line, int column, String msg) {
        System.out.println("error: " + line + ":" + column + " " + msg);
        errorCount++;
    }

    public void display(int line, String msg) {
        System.out.println("error: " + line + " " + msg);
        errorCount++;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
        errorCount++;
    }
}
