package irt;

import temp.Label;

import java.util.LinkedList;

import static irt.Relop.*;

/**
 * @author marafat
 */

public class CJUMP extends Stm {

    public Relop op;
    public Exp left, right;
    public Label iftrue, iffalse;

    public CJUMP(Relop op, Exp left, Exp right, Label iftrue, Label iffalse) {
        this.op = op;
        this.left = left;
        this.right = right;
        this.iftrue = iftrue;
        this.iffalse = iffalse;
    }

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<>();
        children.addFirst(left);
        children.addLast(right);
        return children;
    }

    public Stm build(LinkedList<Exp> children) {
        return new CJUMP(op, children.getFirst(), children.getLast(), iftrue, iffalse);
    }

    public static Relop notRel(Relop op) {
        switch (op) {
            case EQ:  return NE;
            case NE:  return EQ;
            case LT:  return GE;
            case GE:  return LT;
            case GT:  return LE;
            case LE:  return GT;
            case ULT: return UGE;
            case UGE: return ULT;
            case UGT: return ULE;
            case ULE: return UGT;
            default: throw new Error("bad relop in CJUMP.notRel");
        }
    }
}
