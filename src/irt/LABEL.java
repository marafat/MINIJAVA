package irt;

import temp.Label;
import translate.Ex;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class LABEL extends Stm {

    public Label label;

    public LABEL(Label label) {this.label = label;}

    public LinkedList<Exp> children() {return new LinkedList<>();}

    public Stm build(LinkedList<Exp> children) {
        return this;
    }
}
