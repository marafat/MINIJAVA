package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class MOVE extends Stm {

    public Exp dst, src;

    public MOVE(Exp dst, Exp src) {
        this.dst = dst;
        this.src = src;
    }

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<>();
        if (dst instanceof MEM) {
            children.addFirst(((MEM) dst).exp);
            children.addLast(src);
        }
        else
            children.addFirst(src);

        return children;
    }

    public Stm build(LinkedList<Exp> children) {
        if (dst instanceof MEM)
            return new MOVE(new MEM(children.getFirst()), children.getLast());
        else
            return new MOVE(dst, children.getFirst());
    }
}
