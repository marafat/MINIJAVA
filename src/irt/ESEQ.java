package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class ESEQ extends Exp {

    public Stm stm;
    public Exp exp;

    public ESEQ(Stm stm, Exp exp) {
        this.stm = stm;
        this.exp = exp;}

    public LinkedList<Exp> children() {throw new Error("kids() not applicable to ESEQ");}

    public Exp build(LinkedList<Exp> kids) {throw new Error("build() not applicable to ESEQ");}
}
