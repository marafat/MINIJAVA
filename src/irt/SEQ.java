package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class SEQ extends Stm {

    public Stm left, right;

    public SEQ(Stm left, Stm right) {
        this.left = left;
        this.right = right;
    }

    public LinkedList<Exp> children() {throw new Error("kids() not applicable to SEQ");}

    public Stm build(LinkedList<Exp> children) {throw new Error("build() not applicable to SEQ");}
}