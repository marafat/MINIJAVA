package irt;

/**
 * @author arafat
 */

public enum Relop {

    EQ,
    NE,
    LT,
    GT,
    LE,
    GE,
    ULT,
    ULE,
    UGT,
    UGE
}
