package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public abstract class Stm {

    public abstract LinkedList<Exp> children();
    public abstract Stm build(LinkedList<Exp> children);
}
