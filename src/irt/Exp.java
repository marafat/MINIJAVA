package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public abstract class Exp {

    public abstract LinkedList<Exp> children();
    public abstract Exp build(LinkedList<Exp> children);
}
