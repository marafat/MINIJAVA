package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class BINOP extends Exp {

    public Binop op;
    public Exp left, right;


    public BINOP(Binop op, Exp left, Exp right) {
        this.op = op;
        this.left = left;
        this.right = right;
    }

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<>();
        children.addFirst(left);
        children.addLast(right);
        return children;
    }

    public Exp build(LinkedList<Exp> children) {
        return new BINOP(op, children.getFirst(), children.getLast());
    }
}
