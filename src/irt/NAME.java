package irt;

import temp.Label;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class NAME extends Exp {

    public Label label;

    public NAME(Label label) {
        this.label = label;
    }

    public LinkedList<Exp> children() {return new LinkedList<>();}

    public Exp build(LinkedList<Exp> children) {return this;}
}
