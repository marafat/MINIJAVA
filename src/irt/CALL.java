package irt;

import java.util.LinkedList;
import java.util.List;

/**
 * @author marafat
 */

public class CALL extends Exp {

    public Exp func;
    public List<Exp> args;

    public CALL(Exp func, List<Exp> args) {
        this.func=func;
        this.args=args;
    }

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<>();
        children.addFirst(func);
        children.addAll(args);
        return children;
    }

    public Exp build(LinkedList<Exp> children) {
        LinkedList<Exp> c = new LinkedList<>(children);
        return new CALL(c.removeFirst(), c);
    }

}
