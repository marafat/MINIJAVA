package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class CONST extends Exp {

    public int value;

    public CONST(int value) {
        this.value = value;
    }

    public LinkedList<Exp> children() {return new LinkedList<Exp>();}

    public Exp build(LinkedList<Exp> kids) {return this;}
}
