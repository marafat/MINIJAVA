package irt;

import temp.Temp;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class TEMP extends Exp {

    public Temp temp;

    public TEMP(Temp temp) {this.temp = temp;}

    public LinkedList<Exp> children() {return new LinkedList<>();}

    public Exp build(LinkedList<Exp> children) {return this;}
}

