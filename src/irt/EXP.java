package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class EXP extends Stm {

    public Exp exp;

    public EXP(Exp exp) {this.exp = exp;}

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<>();
        children.addFirst(exp);
        return children;
    }

    public Stm build(LinkedList<Exp> children) {
        return new EXP(children.getFirst());
    }
}
