package irt;

import temp.Label;
import temp.LabelList;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @author marafat
 */

public class JUMP extends Stm {

    public Exp exp;
    public LinkedList<Label> targets;

    public JUMP(Exp exp, LinkedList<Label> targets) {
        this.exp = exp;
        this.targets = targets;}

    public JUMP(Label target) {
        exp = new NAME(target);
        targets = new LinkedList<>();
        targets.addFirst(target);

    }

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<Exp>();
        children.addFirst(exp);
        return children;
    }

    public Stm build(LinkedList<Exp> children) {
        return new JUMP(children.getFirst(), targets);
    }
}
