package irt;

import java.util.LinkedList;

/**
 * @author marafat
 */

public class MEM extends Exp {

    public Exp exp;

    public MEM(Exp exp) {this.exp = exp;}

    public LinkedList<Exp> children() {
        LinkedList<Exp> children = new LinkedList<>();
        children.addFirst(exp);
        return children;
    }

    public Exp build(LinkedList<Exp> children) {
        return new MEM(children.getFirst());
    }
}
