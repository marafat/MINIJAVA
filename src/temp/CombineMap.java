package temp;

/**
 * @author marafat
 */

public class CombineMap implements TempMap {

    private TempMap tmap1, tmap2;

    public CombineMap(TempMap tmap1, TempMap tmap2) {
        this.tmap1 = tmap1;
        this.tmap2 = tmap2;
    }

    public String tempMap(Temp t) {
        String s = tmap1.tempMap(t);
        if (s!=null)
            return s;
        return tmap2.tempMap(t);
    }
}
