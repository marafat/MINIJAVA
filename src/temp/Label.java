package temp;

/**
 * @author marafat
 */

import symbol.Symbol;

/**
 * A Label represents an address in assembly language.
 */

public class Label  {

    private static int count;
    private String name;

    /**
     * Makes a new label with an arbitrary name.
     */
    public Label() {
        this("L" + count++);
    }

    /**
     * Makes a new label that prints as "name".
     * Warning: avoid repeated calls to <tt>new Label(s)</tt> with
     * the same name <tt>s</tt>.
     */
    public Label(String n) {
        name = n;
    }

    /**
     * Makes a new label whose name is the same as a symbol.
     */
    public Label(Symbol s) {
        this(s.toString());
    }

    /**
     * a printable representation of the label, for use in assembly
     * language output.
     */
    public String toString() {return name;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Label label = (Label) o;

        return name.equals(label.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
