package temp;

/**
 * @author marafat
 */

public class Temp  {

    private static int count = 30;
    private int num;

    public Temp() {
        num = count++;
    }

    public Temp(int num){ this.num = num ; }

    public String toString() { return "t" + num; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Temp temp = (Temp) o;

        return num == temp.num;
    }

    @Override
    public int hashCode() {
        return num;
    }
}
