package target.mips;

import frame.Access;
import temp.Label;
import temp.Temp;
import irt.*;

import java.util.*;

/**
 * @author marafat
 */

public class Frame extends frame.Frame {

    static final Temp ZERO = new Temp(); // zero reg
    static final Temp AT = new Temp(); // reserved for assembler
    static final Temp V0 = new Temp(); // function result
    static final Temp V1 = new Temp(); // second function result
    static final Temp A0 = new Temp(); // argument1
    static final Temp A1 = new Temp(); // argument2
    static final Temp A2 = new Temp(); // argument3
    static final Temp A3 = new Temp(); // argument4
    static final Temp T0 = new Temp(); // caller-saved
    static final Temp T1 = new Temp();
    static final Temp T2 = new Temp();
    static final Temp T3 = new Temp();
    static final Temp T4 = new Temp();
    static final Temp T5 = new Temp();
    static final Temp T6 = new Temp();
    static final Temp T7 = new Temp();
    static final Temp S0 = new Temp(); // callee-saved
    static final Temp S1 = new Temp();
    static final Temp S2 = new Temp();
    static final Temp S3 = new Temp();
    static final Temp S4 = new Temp();
    static final Temp S5 = new Temp();
    static final Temp S6 = new Temp();
    static final Temp S7 = new Temp();
    static final Temp T8 = new Temp(); // caller-saved
    static final Temp T9 = new Temp();
    static final Temp K0 = new Temp(); // reserved for OS kernel
    static final Temp K1 = new Temp(); // reserved for OS kernel
    static final Temp GP = new Temp(); // pointer to global area
    static final Temp SP = new Temp(); // stack pointer
    static final Temp S8 = new Temp(); // callee-save (frame pointer)
    static final Temp RA = new Temp(); // return address

    private static final
    Map<Temp,String> tempMap = new HashMap<Temp,String>(32);
    static {
        tempMap.put(ZERO, "$0");
        tempMap.put(AT,   "$at");
        tempMap.put(V0,   "$v0");
        tempMap.put(V1,   "$v1");
        tempMap.put(A0,   "$a0");
        tempMap.put(A1,   "$a1");
        tempMap.put(A2,   "$a2");
        tempMap.put(A3,   "$a3");
        tempMap.put(T0,   "$t0");
        tempMap.put(T1,   "$t1");
        tempMap.put(T2,   "$t2");
        tempMap.put(T3,   "$t3");
        tempMap.put(T4,   "$t4");
        tempMap.put(T5,   "$t5");
        tempMap.put(T6,   "$t6");
        tempMap.put(T7,   "$t7");
        tempMap.put(S0,   "$s0");
        tempMap.put(S1,   "$s1");
        tempMap.put(S2,   "$s2");
        tempMap.put(S3,   "$s3");
        tempMap.put(S4,   "$s4");
        tempMap.put(S5,   "$s5");
        tempMap.put(S6,   "$s6");
        tempMap.put(S7,   "$s7");
        tempMap.put(T8,   "$t8");
        tempMap.put(T9,   "$t9");
        tempMap.put(K0,   "$k0");
        tempMap.put(K1,   "$k1");
        tempMap.put(GP,   "$gp");
        tempMap.put(SP,   "$sp");
        tempMap.put(S8,   "$fp");
        tempMap.put(RA,   "$ra");
    }

    private static final Temp[]
            SPECIAL_REGS = { ZERO, AT, K0, K1, GP, SP },
            ARGS_REGS = { A0, A1, A2, A3 },
            CALLEE_SAVED_REGS = { RA, S0, S1, S2, S3, S4, S5, S6, S7, S8 },
            CALLER_SAVED_REGS = { T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, V0, V1 };

    private static List<Temp[]> regs = new ArrayList<>();
    {
        regs.add(SPECIAL_REGS);
        regs.add(ARGS_REGS);
        regs.add(CALLEE_SAVED_REGS);
        regs.add(CALLER_SAVED_REGS);
    }

    private static final int WORDSIZE = 4;
    private static final Temp FP =  new Temp();
    private static Map<Label,Integer> functions = new HashMap<Label,Integer>();
    private static Map<String,Label> stringToLabel = new HashMap<String,Label>();

    private int offset;
    private List<Access> actuals;

    public Frame() {}

    private Frame(Label name,  List<Boolean> formals) {
        this.offset = 0;
        Integer count = functions.get(name);
        if (count == null) {
            count = 0;
            this.name = name;
        } else {
            count = count + 1;
            this.name = new Label(name + "." + count);
        }
        functions.put(name, count);

        this.actuals = new LinkedList<Access>();
        this.formals = new LinkedList<Access>();

        Iterator<Boolean> escapes = formals.iterator();
        this.formals.add(allocLocal(escapes.next()));
        this.actuals.add(new InReg(V0));
        int offset = 0;

        //Assign the first four to the argument regs(actual) if do not escape
        for (Temp ar: ARGS_REGS) {
            if (!escapes.hasNext())
                break;
            offset += WORDSIZE;
            actuals.add(new InReg(ar));
            if (escapes.next())
                this.formals.add(new InFrame(offset));
            else
                this.formals.add(new InReg(new Temp()));
        }

        //Assign the remainings to the temporaries if do not escape
        while (escapes.hasNext()) {
            offset += WORDSIZE;
            Access actual = new InFrame(offset);
            actuals.add(actual);
            if (escapes.next())
                this.formals.add(actual);
            else
                this.formals.add(new InReg(new Temp()));
        }
    }

    @Override
    public frame.Frame newFrame(Label name, List<Boolean> formals) {
        if (this.name != null)
            name = new Label(this.name + "." + name);  //for nested functions
        return new Frame(name, formals);
    }

    @Override
    public Access allocLocal(boolean escape) {
        if (!escape)
            return new InReg(new Temp());
        Access inframe = new InFrame(offset);
        offset -= WORDSIZE;
        return inframe;
    }

    @Override
    public Temp FP() {
        return FP;
    }

    @Override
    public Temp RV() {
        return V0;
    }

    @Override
    public String tempMap(Temp temp) {
        return tempMap.get(temp);
    }

    @Override
    public List<Temp[]> registers() { return regs;}

    @Override
    public int wordSize() {
        return WORDSIZE;
    }

    @Override
    public Exp externalCall(String func, List<Exp> args) {
        Label l = stringToLabel.get(func);
        if (l == null) {
            l = new Label("_" + func);
            stringToLabel.put(func, l);
        }

        return new CALL(new NAME(l), args);
    }

    @Override
    public String programTail(){

        return
                "         .text            \n" +
                        "         .globl _halloc   \n" +
                        "_halloc:                  \n" +
                        "         li $v0, 9        \n" +
                        "         syscall          \n" +
                        "         j $ra            \n" +
                        "                          \n" +
                        "         .text            \n" +
                        "         .globl _printint \n" +
                        "_printint:                \n" +
                        "         li $v0, 1        \n" +
                        "         syscall          \n" +
                        "         la $a0, newl     \n" +
                        "         li $v0, 4        \n" +
                        "         syscall          \n" +
                        "         j $ra            \n" +
                        "                          \n" +
                        "         .data            \n" +
                        "         .align   0       \n" +
                        "newl:    .asciiz \"\\n\"  \n" +
                        "         .data            \n" +
                        "         .align   0       \n" +
                        "str_er:  .asciiz \" ERROR: abnormal termination\\n\" "+
                        "                          \n" +
                        "         .text            \n" +
                        "         .globl _error    \n" +
                        "_error:                   \n" +
                        "         li $v0, 4        \n" +
                        "         la $a0, str_er   \n" +
                        "         syscall          \n" +
                        "         li $v0, 10       \n" +
                        "         syscall          \n" ;
    }

}
