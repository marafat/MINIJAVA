package target.mips;

import irt.*;

/**
 * @author marafat
 */

class InFrame extends frame.Access {

    public int offset;

    public InFrame(int offset) {
        this.offset = offset;
    }

    @Override
    public Exp exp(Exp framePtr) {
        return new MEM(new BINOP(Binop.PLUS, framePtr, new CONST(offset)));
    }

    @Override
    public String toString() {
        Integer offset = new Integer(this.offset);
        return offset.toString();
    }
}
