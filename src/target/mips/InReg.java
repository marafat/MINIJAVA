package target.mips;

import temp.Temp;
import irt.*;

/**
 * @author marafat
 */

class InReg extends frame.Access {

    public Temp temp;

    public InReg(Temp temp) {
        this.temp = temp;
    }

    @Override
    public Exp exp(Exp framePtr) {
        return new TEMP(temp);
    }

    @Override
    public String toString() {
        return temp.toString();
    }
}
