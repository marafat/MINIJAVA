package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class BooleanTrueLiteral extends Expression {

    public BooleanTrueLiteral(int line, int column) {
        super(line, column);
    }

    @Override
    public String toString() {
        return "BOOLEAN";
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
