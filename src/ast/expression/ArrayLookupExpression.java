package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ArrayLookupExpression extends Expression {

    public Expression ref;
    public Expression ss;

    public ArrayLookupExpression(Expression ref, Expression ss, int line, int column) {
        super(line, column);
        this.ref = ref;
        this.ss = ss;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
