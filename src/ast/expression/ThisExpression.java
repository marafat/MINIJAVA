package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ThisExpression extends Expression {

    public ThisExpression(int line, int column) {
        super(line, column);
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
