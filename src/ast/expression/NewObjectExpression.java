package ast.expression;

import ast.Identifier;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class NewObjectExpression extends Expression {

    public Identifier id;

    public NewObjectExpression(Identifier id, int line, int column) {
        super(line, column);
        this.id = id;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
