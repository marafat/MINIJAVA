package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ArrayLengthExpression extends Expression {

    public Expression ref;

    public ArrayLengthExpression(Expression ref, int line, int column) {
        super(line, column);
        this.ref = ref;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
