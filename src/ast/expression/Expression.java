package ast.expression;

import ast.Node;

/**
 * @author marafat
 */

public abstract class Expression extends Node {

    public Expression(int line, int column) {
        super(line, column);
    }
}
