package ast.expression;

import ast.Identifier;
import visitor.AbstractVisitor;

import java.util.List;

/**
 * @author marafat
 */

public class CallExpression extends Expression {

    public Expression ref;
    public Identifier id;
    public List<Expression> al;

    public CallExpression(Expression ref, Identifier id, List<Expression> al, int line, int column) {
        super(line, column);
        this.ref = ref;
        this.id = id;
        this.al = al;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
