package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class NotExpression extends Expression {

    public Expression e;

    public NotExpression(Expression e, int line, int column) {
        super(line, column);
        this.e = e;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
