package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ArithmeticExpression extends Expression {

    public Expression le;
    public int op;
    public Expression re;

    public ArithmeticExpression(Expression le, int op, Expression re, int line, int column) {
        super(line, column);
        this.le = le;
        this.op = op;
        this.re = re;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
