package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class IdentifierExpression extends Expression {

    public String n;

    public IdentifierExpression(String n, int line, int column) {
        super(line, column);
        this.n = n;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
