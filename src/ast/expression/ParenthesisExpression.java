package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ParenthesisExpression extends Expression {

    public Expression e;

    public ParenthesisExpression(Expression e, int line, int column) {
        super(line, column);
        this.e = e;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
