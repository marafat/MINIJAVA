package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class IntegerLiteral extends Expression {

    public int value;

    public IntegerLiteral(int value, int line, int column) {
        super(line, column);
        this.value = value;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
