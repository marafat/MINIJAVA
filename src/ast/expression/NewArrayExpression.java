package ast.expression;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class NewArrayExpression extends Expression {

    public Expression size;

    public NewArrayExpression(Expression size, int line, int column) {
        super(line, column);
        this.size = size;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
