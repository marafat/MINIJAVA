package ast;

/**
 * @author marafat
 */

public abstract class Type extends Node {

    public Type(int line, int column) {
        super(line, column);
    }
}
