package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class IdentifierType extends Type {

    public String n;

    public IdentifierType(String n, int line, int column) {
        super(line, column);
        this.n = n;
    }

    @Override
    public String toString() {
        return "IDENTIFIER";
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
