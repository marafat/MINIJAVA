package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class IntArrayType extends Type {

    public IntArrayType(int line, int column) {
        super(line, column);
    }

    @Override
    public String toString() {
        return "INTARRAY";
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
