package ast;

import visitor.AbstractVisitor;

import java.util.List;

/**
 * @author marafat
 */

public class ClassBody extends Node {

    public List<VarDeclaration> vd;
    public List<MethodDeclaration> md;

    public ClassBody(List<VarDeclaration> vd, List<MethodDeclaration> md, int line, int column) {
        super(line, column);
        this.vd = vd;
        this.md = md;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
