package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class IntegerType extends Type {

    public IntegerType(int line, int column) {
        super(line, column);
    }

    @Override
    public String toString() {
        return "INTEGER";
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}

