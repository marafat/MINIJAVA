package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class MainClass extends Node {

    public Identifier id;
    public MainMethodDeclaration mmd;

    public MainClass(Identifier id, MainMethodDeclaration mmd, int line, int column) {
        super(line, column);
        this.id = id;
        this.mmd = mmd;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
