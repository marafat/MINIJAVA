package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class MainMethodDeclaration extends Node {

    public Identifier id;
    public MainMethodBody mmb;

    public MainMethodDeclaration(Identifier id, MainMethodBody mmb, int line, int column) {
        super(line, column);
        this.id = id;
        this.mmb = mmb;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
