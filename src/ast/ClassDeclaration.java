package ast;

/**
 * @author marafat
 */

public abstract class ClassDeclaration extends Node {

    public ClassDeclaration(int line, int column) {
        super(line, column);
    }

}
