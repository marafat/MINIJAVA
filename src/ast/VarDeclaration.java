package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class VarDeclaration extends Node {

    public Type t;
    public Identifier id;

    public VarDeclaration(Type t, Identifier id, int line, int column) {
        super(line, column);
        this.t = t;
        this.id = id;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
