package ast;

import ast.expression.Expression;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class Identifier extends Node {

    public String n;

    public Identifier(String n, int line, int column) {
        super(line, column);
        this.n = n;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
