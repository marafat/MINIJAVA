package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ClassDeclarationExtends extends ast.ClassDeclaration {

    public Identifier ids;
    public Identifier idx;
    public ClassBody cb;

    public ClassDeclarationExtends(Identifier ids, Identifier idx, ClassBody cb, int line, int column) {
        super(line, column);
        this.ids = ids;
        this.idx = idx;
        this.cb = cb;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
