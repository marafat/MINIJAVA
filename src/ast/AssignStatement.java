package ast;

import ast.expression.Expression;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class AssignStatement extends Statement {

    public Identifier id;
    public Expression e;

    public AssignStatement(Identifier id, Expression e, int line, int column) {
        super(line, column);
        this.id = id;
        this.e = e;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
