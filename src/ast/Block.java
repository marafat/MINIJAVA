package ast;

import visitor.AbstractVisitor;

import java.util.List;

/**
 * @author marafat
 */

public class Block extends Statement {

    public List<Statement> sl;

    public Block(List<Statement> sl, int line, int column) {
        super(line, column);
        this.sl = sl;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
