package ast;

import visitor.AbstractVisitor;

import java.util.List;

/**
 * @author marafat
 */

public class MethodDeclaration extends Node {

    public Type t;
    public Identifier id;
    public ParameterList pl;
    public MethodBody mb;

    public MethodDeclaration(Type t, Identifier id, ParameterList pl, MethodBody mb, int line, int column) {
        super(line, column);
        this.t = t;
        this.id = id;
        this.pl = pl;
        this.mb = mb;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
