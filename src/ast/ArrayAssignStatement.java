package ast;

import ast.expression.*;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ArrayAssignStatement extends Statement{

    public Identifier id;
    public Expression e1;
    public Expression e2;

    public ArrayAssignStatement(Identifier id, Expression e1, Expression e2, int line, int column) {
        super(line, column);
        this.id = id;
        this.e1 = e1;
        this.e2 = e2;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
