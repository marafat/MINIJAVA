package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class MainMethodBody extends Node {

    public Statement s;

    public MainMethodBody(Statement s, int line, int column) {
        super(line, column);
        this.s = s;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
