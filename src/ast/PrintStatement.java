package ast;

import ast.expression.*;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class PrintStatement extends Statement{

    public Expression e;

    public PrintStatement(Expression e, int line, int column) {
        super(line, column);
        this.e = e;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
