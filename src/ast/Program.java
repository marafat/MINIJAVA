package ast;

import visitor.AbstractVisitor;

import java.util.List;

/**
 * @author marafat
 */

public class Program extends Node {

    public MainClass mc;
    public List<ClassDeclaration> cd;

    public Program(MainClass mc, List<ClassDeclaration> cd, int line, int column) {
        super(line, column);
        this.mc = mc;
        this.cd = cd;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
