package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public abstract class Statement extends Node {

    public Statement(int line, int column) {
        super(line, column);
    }
}
