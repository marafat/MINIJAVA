package ast;

import ast.expression.*;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class IfStatement extends Statement {

    public Expression e;
    public Statement s1;
    public Statement s2;

    public IfStatement(Expression e, Statement s1, Statement s2, int line, int column) {
        super(line, column);
        this.e = e;
        this.s1 = s1;
        this.s2 = s2;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
