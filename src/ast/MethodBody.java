package ast;

import java.util.List;
import ast.expression.*;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class MethodBody extends Node {

    public List<VarDeclaration> vdl;
    public List<Statement> sl;
    public Expression re;

    public MethodBody(List<VarDeclaration> vdl, List<Statement> sl, Expression re, int line, int column) {
        super(line, column);
        this.vdl = vdl;
        this.sl = sl;
        this.re = re;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
