package ast;

import visitor.AbstractVisitor;

import java.util.List;

/**
 * @author marafat
 */

public class ParameterList extends Node {

    public List<Type> tl;
    public List<Identifier> idl;

    public ParameterList(List<Type> tl, List<Identifier> idl, int line, int column) {
        super(line, column);
        this.tl = tl;
        this.idl = idl;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
