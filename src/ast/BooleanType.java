package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class BooleanType extends Type {

    public BooleanType(int line, int column) {
        super(line, column);
    }

    @Override
    public String toString() {
        return "BOOLEAN";
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
