package ast;

import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class ClassDeclarationSimple extends ClassDeclaration {

    public Identifier id;
    public ClassBody cb;

    public ClassDeclarationSimple(Identifier id, ClassBody cb, int line, int column) {
        super(line, column);
        this.id = id;
        this.cb = cb;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
