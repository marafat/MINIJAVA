package ast;

import ast.expression.*;
import visitor.AbstractVisitor;

/**
 * @author marafat
 */

public class WhileStatement extends Statement {

    public Expression e;
    public Statement s;

    public WhileStatement(Expression e, Statement s, int line, int column) {
        super(line, column);
        this.e = e;
        this.s = s;
    }

    public <R> R accept(AbstractVisitor<R> v) {
        return v.visit(this);
    }
}
