import ast.Node;
import frontend.*;
import org.antlr.v4.runtime.*;
import symbol.ClassBinding;
import symbol.Symbol;
import temp.Temp;
import translate.Frag;
import translate.ProcFrag;
import visitor.ASTBuildingVisitor;
import visitor.GTableBuildingVisitor;
import visitor.IRGeneratingVisitor;
import visitor.TypeCheckingVisitor;

import java.io.File;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;

/**
 * @author marafat
 */

public class Main {

    public static void main(String [] args) throws Exception {
        //Lexing
        final String file = new File(args[0]).getAbsolutePath();
        final MiniJavaLexer lexer = new MiniJavaLexer(CharStreams.fromFileName(file));
        final Vocabulary vocab = lexer.getVocabulary();

        //Print all lexical tokens in the program
//        for(final Token token: lexer.getAllTokens()) {
//            if (token.getType() == Token.EOF)
//                break;
//            System.out.println(vocab.getSymbolicName(token.getType()));
//        }

        //Parsing
        final CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        final MiniJavaParser parser = new MiniJavaParser(tokenStream);
        final error.Error error = new error.Error(); parser.addErrorListener(error);
        final MiniJavaParser.ProgramContext pctx = parser.program();
        if (error.getCount() > 0)
            System.exit(1);

        final Node ast = pctx.accept(new ASTBuildingVisitor());
        //ast.accept(new PrettyPrintVisitor());


        //Semantic Analysis
        final GTableBuildingVisitor semanticPass1 = new GTableBuildingVisitor();
        final Map<Symbol, ClassBinding> forwardDecls = semanticPass1.traverse(ast);
        final TypeCheckingVisitor semanticPass2 = new TypeCheckingVisitor(forwardDecls);
        semanticPass2.traverse(ast);

        //IR Generation
        final IRGeneratingVisitor irGen = new IRGeneratingVisitor(new target.mips.Frame(), forwardDecls);
        irGen.traverse(ast);

        //Frame frame = new target.mips.Frame();
        //allocate a new frame
        //frame.newFrame(g,new BoolList(true,
        //                                   new BoolList(false,
        //                                                     new BoolList(false, null))))

        //frame.allocLocal(false) //allocate new local variable in frame

        PrintWriter debug =	new PrintWriter(System.out);
        for (Frag f: irGen.getResult()) {
            if (f instanceof ProcFrag){
                temp.TempMap tempmap = new temp.CombineMap(((ProcFrag)f).frame, new temp.DefaultMap());

                debug.println("PROCEDURE :" + ((ProcFrag)f).frame.name);

                if (((ProcFrag)f).body != null) {
                    new Print(debug, tempmap, ((ProcFrag)f).body);
                }
            }
        }
        debug.flush();
    }

}

