package translate;

/**
 * @author marafat
 */

public abstract class Expr {

    public abstract irt.Exp unEx();
    public abstract irt.Stm unNx();
    public abstract irt.Stm unCx(temp.Label t, temp.Label f);

}
