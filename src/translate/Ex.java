package translate;

import irt.*;
import temp.Label;

/**
 * @author marafat
 */

public class Ex extends Expr {

    private irt.Exp exp;

    public Ex(irt.Exp exp) {this.exp = exp;}

    public irt.Exp unEx() {return exp;}

    public irt.Stm unNx() {return new irt.EXP(exp);}

    public irt.Stm unCx(Label t, Label f) {
        if (exp instanceof CONST)
            if (((CONST)exp).value != 0)
                return new JUMP(t);
            else
                return new JUMP(f);

        return new CJUMP(Relop.NE, exp, new CONST(0), t, f);
    }
}
