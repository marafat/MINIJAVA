package translate;

import temp.Label;
import  irt.*;
import temp.Temp;

/**
 * @author marafat
 */

public class IfThenElseExp extends Expr{

    private Expr cond, e1, e2;
    private Label t = new Label();
    private Label f = new Label();
    private Label join = new Label();

    public IfThenElseExp(Expr cond, Expr e1, Expr e2) {
        this.cond = cond;
        this.e1 = e1;
        this.e2 = e2;
    }

    public Exp unEx() {
        Temp r = new Temp();
        return new ESEQ(new SEQ(cond.unCx(t, f),
                                new SEQ(new LABEL(t),
                                        new SEQ(new MOVE(new TEMP(r), e1.unEx()),
                                                new SEQ(new JUMP(join),
                                                        new SEQ(new LABEL(f),
                                                                new SEQ(new MOVE(new TEMP(r), e2.unEx()),
                                                                        new JUMP(join))))))),
                        new TEMP(r));
    }

    public Stm unNx() {
        return new SEQ(cond.unCx(t, f),
                       new SEQ(new LABEL(t),
                               new SEQ(e1.unNx(),
                                       new SEQ(new JUMP(join),
                                               new SEQ(new LABEL(f),
                                                       new SEQ(e2.unNx(),
                                                               new JUMP(join)))))));
    }

    public Stm unCx(Label tt, Label ff) {
        return new SEQ(cond.unCx(t, f),
                       new SEQ(new LABEL(t),
                               new SEQ(e1.unCx(tt, ff),
                                       new SEQ(new LABEL(f), e2.unCx(tt, ff)))));
    }
}
