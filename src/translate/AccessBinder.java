package translate;

/**
 * @author marafat
 */

public class AccessBinder {

    Level base;
    frame.Access access;

    AccessBinder(Level base, frame.Access access) {
        this.base = base;
        this.access = access;
    }

    public String toString() {
        return "[" + base.frame.name.toString() + ","
                + access.toString() + "]";
    }
}
