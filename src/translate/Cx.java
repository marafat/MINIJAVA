package translate;

import irt.*;
import temp.Label;
import temp.Temp;

/**
 * @author marafat
 */

public abstract class Cx extends Expr {

    public irt.Exp unEx() {
        Temp r = new Temp();
        Label t = new Label();
        Label f = new Label();
        return new ESEQ(new SEQ(new MOVE(new TEMP(r),
                                          new CONST(1)),
                                 new SEQ(this.unCx(t,f),
                                          new SEQ(new LABEL(f),
                                                   new SEQ(new MOVE(new TEMP(r),
                                                                new CONST(0)),
                                                           new LABEL(t))))),
                         new irt.TEMP(r));
    }

    public irt.Stm unNx() {
        Label j = new Label();
        return new SEQ(unCx(j, j), new LABEL(j));
    }

    public abstract irt.Stm unCx(Label t, Label f);
}