package translate;

import irt.Relop;
import temp.Label;

/**
 * @author marafat
 */

public class RelCx extends Cx {

    private Relop op;
    private irt.Exp left;
    private irt.Exp right;

    public RelCx(irt.Exp left, Relop op, irt.Exp right) {
        this.left = left;
        this.op = op;
        this.right = right;
    }

    @Override
    public irt.Stm unCx(Label t, Label f) {
        return new irt.CJUMP(op, left, right, t, f);
    }
}
