package translate;

import frame.Frame;
import temp.Label;

import java.util.LinkedList;
import java.util.List;

/**
 * @author marafat
 */

public class Level {

    public Frame frame;
    public Level parent;
    public List<AccessBinder> formals = new LinkedList<AccessBinder>();

    public Level(Frame frame) { this.frame = frame; }

    public Level(Level parent, Label name, List<Boolean> formals) {
        this.parent = parent;
        frame = parent.frame.newFrame(name, formals);

        for (frame.Access f: frame.formals)
            this.formals.add(new AccessBinder(this, f));
    }

    public Label name() { return frame.name; }

    public AccessBinder allocLocal(boolean escape) {
        return new AccessBinder(this, frame.allocLocal(escape));
    }

}
