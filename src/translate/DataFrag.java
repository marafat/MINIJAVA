package translate;

/**
 * @author marafat
 */

public class DataFrag extends Frag {

    public String data;

    public DataFrag(String data) {
        this.data = data;
    }

    public String toString() { return data; }
}
