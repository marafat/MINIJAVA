package translate;

import irt.*;
/**
 * @author marafat
 */

public class ProcFrag extends Frag {

    public Stm body;
    public frame.Frame frame;

    public ProcFrag(Stm body, frame.Frame frame) {
        this.body = body;
        this.frame = frame;
    }
}
