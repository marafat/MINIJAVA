package translate;

import irt.CONST;
import irt.ESEQ;
import temp.Label;

/**
 * @author marafat
 */

public class Nx extends Expr {

    private irt.Stm stm;

    public Nx(irt.Stm stm) {this.stm = stm;}

    public irt.Exp unEx() {return null;}

    public irt.Stm unNx() {return stm;}

    public irt.Stm unCx(Label t, Label f) {return  null;}
}
