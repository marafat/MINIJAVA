This is an implementaion compiler for **MiniJava** Language taken from W. Appel and J. Palsberg's [Modern Compiler Implementation](http://www.cambridge.org/us/catalogue/catalogue.asp?isbn=052182060x), pages 484-486)

**MiniJava** is a subset of Java. The meaning of a MiniJava program is given by its meaning as a Java program. Overloading is not allowed in MiniJava. The MiniJava statement System.out.println( ... ); can only print integers. The MiniJava expression e.length only applies to expressions of type int [].

**DEVELOPMENT**  
The frontend of the compiler(lexer and parser) is implemented in Java using [Antlr](http://www.antlr.org/). The backend(optimization and code generation) translates the AST to an intermediate representation which can be directly translated to the instruction set of any platform, for example: ARM, MIPS, x86. *The backend is still a work in progress*.

**GRAMMAR**
```
Program ::= MainClass ( ClassDeclaration )* <EOF>

MainClass ::= "class" Identifier "{" "public" "static" "void" "main" "(" "String" "[" "]" Identifier ")" "{" Statement "}" "}"

ClassDeclaration ::= "class" Identifier ( "extends" Identifier )? "{" ( VarDeclaration )* ( MethodDeclaration )* "}"

VarDeclaration ::= Type Identifier ";"

MethodDeclaration ::= "public" Type Identifier "(" ( Type Identifier ( "," Type Identifier )* )? ")" "{" ( VarDeclaration )* ( Statement )* "return" Expression ";" "}"

Type ::= "int" "[" "]"  
|	"boolean"  
|	"int"  
|	Identifier

Statement ::= "{" ( Statement )* "}"   
|	"if" "(" Expression ")" Statement "else" Statement  
|	"while" "(" Expression ")" Statement  
|	"System.out.println" "(" Expression ")" ";"  
|	Identifier "=" Expression ";"  
|	Identifier "[" Expression "]" "=" Expression ";"

Expression ::= Expression ( "&&" | "<" | "+" | "-" | "*" ) Expression  
|	Expression "[" Expression "]"  
|	Expression "." "length"  
|	Expression "." Identifier "(" ( Expression ( "," Expression )* )? ")"  
|	<INTEGER_LITERAL>  
|	"true"  
|	"false"  
|	Identifier  
|	"this"  
|	"new" "int" "[" Expression "]"  
|	"new" Identifier "(" ")"  
|	"!" Expression  
|	"(" Expression ")"  

Identifier ::= <IDENTIFIER>
```

**BUILD, COMPILE, RUN**  
To build run the following command from commandline. *You should have [Apache Ant](https://ant.apache.org/) installed on your system*
```
#Build
ant

#Clean
ant clean

#Compile a MiniJava Program
jar -cp path/to/compiler-jar Factorial.mjava

#Run
```  

**SAMPLE PROGRAM**
```java

class Factorial{
    public static void main(String[] a){
        System.out.println(new Fac().ComputeFac(10));
    }
}

class Fac {
    public int ComputeFac(int num){
        int num_aux ;
        if (num < 1)
            num_aux = 1 ;
        else
            num_aux = num * (this.ComputeFac(num-1)) ;
        return num_aux ;
    }
}
```
