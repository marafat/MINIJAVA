grammar MiniJava;

/**
 * @author marafat
 */

/*Parser Rules
 for MiniJava
 */

program
    : mainClass classDeclaration* EOF
    ;

mainClass
    : CLASS identifier LBRACE mainMethodDeclaration RBRACE
    ;

mainMethodDeclaration
    : PUBLIC STATIC VOID MAIN LPAREN STRING LBRACKET RBRACKET identifier RPAREN mainMethodBody
    ;

mainMethodBody
    : LBRACE statement RBRACE
    ;

classDeclaration
    : classDeclarationSimple
    | classDeclarationExtends
    ;

classDeclarationSimple
    : CLASS identifier LBRACE classBody RBRACE
    ;

classDeclarationExtends
    : CLASS identifier EXTENDS identifier LBRACE classBody RBRACE
    ;

classBody
    : (varDeclaration)* (methodDeclaration)*
    ;

varDeclaration
    : type identifier SEMI
    ;

methodDeclaration
    : PUBLIC type identifier LPAREN (parameterList)? RPAREN LBRACE methodBody RBRACE
    ;

parameterList
    : type identifier (COMMA type identifier)*
    ;

methodBody
    : (varDeclaration)* (statement)* RETURN expression SEMI
    ;

type
    : intArrayType
    | booleanType
    | integerType
    | identifierType
    ;

intArrayType
    : INT LBRACKET RBRACKET
    ;

booleanType
    : BOOLEAN
    ;

integerType
    : INT
    ;

identifierType
    : ID
    ;

statement
    : block
    | ifStatement
    | whileStatement
    | printStatement
    | assignStatement
    | arrayAssignStatement
    ;

block
    : LBRACE (statement)* RBRACE
    ;

ifStatement
    : IF LPAREN expression RPAREN statement ELSE statement
    ;

whileStatement
    : WHILE LPAREN expression RPAREN statement
    ;

printStatement
    : PRINTLN LPAREN expression RPAREN SEMI
    ;

assignStatement
    : identifier ASSIGN expression SEMI
    ;

arrayAssignStatement
    : identifier LBRACKET expression RBRACKET ASSIGN expression SEMI
    ;

expression
    : expression op=(AND | OR) expression #LogicalExpression
    | expression op=(EQ | LT | LTE) expression #CompareExpression
    | expression op=(ADD | SUB | MUL) expression #ArithmeticExpression
    | expression LBRACKET expression RBRACKET #ArrayLookupExpression
    | expression DOT LENGTH #ArrayLengthExpression
    | expression DOT identifier LPAREN (expression (COMMA expression)*)? RPAREN #CallExpression
    | NEW INT LBRACKET expression RBRACKET #NewArrayExpression
    | NEW identifier LPAREN RPAREN #NewObjectExpression
    | BANG expression #NotExpression
    | LPAREN expression RPAREN #ParenthesisExpression
    | INTEGER_LITERAL #IntegerLiteral
    | TRUE #BooleanTrueLiteral
    | FALSE #BooleanFalseLiteral
    | THIS #ThisExpression
    | ID #IdentifierExpression
    ;

identifier
    : ID
    ;


/*
  Lexer Rules
  for MiniJava
 */

//Keywords
CLASS              : 'class';
PUBLIC             : 'public';
STATIC             : 'static';
VOID               : 'void';
MAIN               : 'main';
STRING             : 'String';
EXTENDS            : 'extends';
RETURN             : 'return';
INT                : 'int';
BOOLEAN            : 'boolean';
IF                 : 'if';
ELSE               : 'else';
WHILE              : 'while';
LENGTH             : 'length';
TRUE               : 'true';
FALSE              : 'false';
THIS               : 'this';
NEW                : 'new';
PRINTLN            : 'System.out.println';

//Comparison Operators
EQ                 : '==';
LT                 : '<';
LTE                : '<=';

//Arithmetic Operators
ADD                : '+';
SUB                : '-';
MUL                : '*';

//Other Operators
BANG               : '!';
AND                : '&&';
OR                 : '||';
ASSIGN             : '=';

//Separators
LBRACE             : '{';
RBRACE             : '}';
LBRACKET           : '[';
RBRACKET           : ']';
LPAREN             : '(';
RPAREN             : ')';
SEMI               : ';';
COMMA              : ',';
DOT                : '.';

//Identifiers
ID                 : [a-zA-Z][a-zA-Z0-9_]*;

//Literals
INTEGER_LITERAL    : Digit | NonZeroDigit Digit*;

//fragment section
fragment
Digit
	:	'0'
	|	NonZeroDigit
	;

fragment
NonZeroDigit
	:	[1-9]
	;

//Comments
SLCOMMENT
    :   '//' ~[\r\n]* -> Skip
    ;

MLCOMMENT
    //:   '/*' .*? '*/' -> channel(HIDDEN)
    :   '/*' .*? '*/' -> Skip
    ;

//Whitespace
WS                 : [ \n\t\r]+ -> Skip;