// Generated from MiniJava.g4 by ANTLR 4.7.1
package frontend;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MiniJavaLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		CLASS=1, PUBLIC=2, STATIC=3, VOID=4, MAIN=5, STRING=6, EXTENDS=7, RETURN=8, 
		INT=9, BOOLEAN=10, IF=11, ELSE=12, WHILE=13, LENGTH=14, TRUE=15, FALSE=16, 
		THIS=17, NEW=18, PRINTLN=19, EQ=20, LT=21, LTE=22, ADD=23, SUB=24, MUL=25, 
		BANG=26, AND=27, OR=28, ASSIGN=29, LBRACE=30, RBRACE=31, LBRACKET=32, 
		RBRACKET=33, LPAREN=34, RPAREN=35, SEMI=36, COMMA=37, DOT=38, ID=39, INTEGER_LITERAL=40, 
		SLCOMMENT=41, MLCOMMENT=42, WS=43;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"CLASS", "PUBLIC", "STATIC", "VOID", "MAIN", "STRING", "EXTENDS", "RETURN", 
		"INT", "BOOLEAN", "IF", "ELSE", "WHILE", "LENGTH", "TRUE", "FALSE", "THIS", 
		"NEW", "PRINTLN", "EQ", "LT", "LTE", "ADD", "SUB", "MUL", "BANG", "AND", 
		"OR", "ASSIGN", "LBRACE", "RBRACE", "LBRACKET", "RBRACKET", "LPAREN", 
		"RPAREN", "SEMI", "COMMA", "DOT", "ID", "INTEGER_LITERAL", "Digit", "NonZeroDigit", 
		"SLCOMMENT", "MLCOMMENT", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'class'", "'public'", "'static'", "'void'", "'main'", "'String'", 
		"'extends'", "'return'", "'int'", "'boolean'", "'if'", "'else'", "'while'", 
		"'length'", "'true'", "'false'", "'this'", "'new'", "'System.out.println'", 
		"'=='", "'<'", "'<='", "'+'", "'-'", "'*'", "'!'", "'&&'", "'||'", "'='", 
		"'{'", "'}'", "'['", "']'", "'('", "')'", "';'", "','", "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "CLASS", "PUBLIC", "STATIC", "VOID", "MAIN", "STRING", "EXTENDS", 
		"RETURN", "INT", "BOOLEAN", "IF", "ELSE", "WHILE", "LENGTH", "TRUE", "FALSE", 
		"THIS", "NEW", "PRINTLN", "EQ", "LT", "LTE", "ADD", "SUB", "MUL", "BANG", 
		"AND", "OR", "ASSIGN", "LBRACE", "RBRACE", "LBRACKET", "RBRACKET", "LPAREN", 
		"RPAREN", "SEMI", "COMMA", "DOT", "ID", "INTEGER_LITERAL", "SLCOMMENT", 
		"MLCOMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MiniJavaLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MiniJava.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 42:
			SLCOMMENT_action((RuleContext)_localctx, actionIndex);
			break;
		case 43:
			MLCOMMENT_action((RuleContext)_localctx, actionIndex);
			break;
		case 44:
			WS_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void SLCOMMENT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			skip();
			break;
		}
	}
	private void MLCOMMENT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			skip();
			break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2:
			skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2-\u013a\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3"+
		"\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\31\3"+
		"\31\3\32\3\32\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36\3\37\3"+
		"\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\7(\u0106"+
		"\n(\f(\16(\u0109\13(\3)\3)\3)\7)\u010e\n)\f)\16)\u0111\13)\5)\u0113\n"+
		")\3*\3*\5*\u0117\n*\3+\3+\3,\3,\3,\3,\7,\u011f\n,\f,\16,\u0122\13,\3,"+
		"\3,\3-\3-\3-\3-\7-\u012a\n-\f-\16-\u012d\13-\3-\3-\3-\3-\3-\3.\6.\u0135"+
		"\n.\r.\16.\u0136\3.\3.\3\u012b\2/\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23"+
		"\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31"+
		"\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S\2U\2W+Y,[-\3"+
		"\2\7\4\2C\\c|\6\2\62;C\\aac|\3\2\63;\4\2\f\f\17\17\5\2\13\f\17\17\"\""+
		"\2\u013e\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2"+
		"\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27"+
		"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2"+
		"\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2"+
		"\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2"+
		"\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2"+
		"\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2W"+
		"\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\3]\3\2\2\2\5c\3\2\2\2\7j\3\2\2\2\tq\3\2"+
		"\2\2\13v\3\2\2\2\r{\3\2\2\2\17\u0082\3\2\2\2\21\u008a\3\2\2\2\23\u0091"+
		"\3\2\2\2\25\u0095\3\2\2\2\27\u009d\3\2\2\2\31\u00a0\3\2\2\2\33\u00a5\3"+
		"\2\2\2\35\u00ab\3\2\2\2\37\u00b2\3\2\2\2!\u00b7\3\2\2\2#\u00bd\3\2\2\2"+
		"%\u00c2\3\2\2\2\'\u00c6\3\2\2\2)\u00d9\3\2\2\2+\u00dc\3\2\2\2-\u00de\3"+
		"\2\2\2/\u00e1\3\2\2\2\61\u00e3\3\2\2\2\63\u00e5\3\2\2\2\65\u00e7\3\2\2"+
		"\2\67\u00e9\3\2\2\29\u00ec\3\2\2\2;\u00ef\3\2\2\2=\u00f1\3\2\2\2?\u00f3"+
		"\3\2\2\2A\u00f5\3\2\2\2C\u00f7\3\2\2\2E\u00f9\3\2\2\2G\u00fb\3\2\2\2I"+
		"\u00fd\3\2\2\2K\u00ff\3\2\2\2M\u0101\3\2\2\2O\u0103\3\2\2\2Q\u0112\3\2"+
		"\2\2S\u0116\3\2\2\2U\u0118\3\2\2\2W\u011a\3\2\2\2Y\u0125\3\2\2\2[\u0134"+
		"\3\2\2\2]^\7e\2\2^_\7n\2\2_`\7c\2\2`a\7u\2\2ab\7u\2\2b\4\3\2\2\2cd\7r"+
		"\2\2de\7w\2\2ef\7d\2\2fg\7n\2\2gh\7k\2\2hi\7e\2\2i\6\3\2\2\2jk\7u\2\2"+
		"kl\7v\2\2lm\7c\2\2mn\7v\2\2no\7k\2\2op\7e\2\2p\b\3\2\2\2qr\7x\2\2rs\7"+
		"q\2\2st\7k\2\2tu\7f\2\2u\n\3\2\2\2vw\7o\2\2wx\7c\2\2xy\7k\2\2yz\7p\2\2"+
		"z\f\3\2\2\2{|\7U\2\2|}\7v\2\2}~\7t\2\2~\177\7k\2\2\177\u0080\7p\2\2\u0080"+
		"\u0081\7i\2\2\u0081\16\3\2\2\2\u0082\u0083\7g\2\2\u0083\u0084\7z\2\2\u0084"+
		"\u0085\7v\2\2\u0085\u0086\7g\2\2\u0086\u0087\7p\2\2\u0087\u0088\7f\2\2"+
		"\u0088\u0089\7u\2\2\u0089\20\3\2\2\2\u008a\u008b\7t\2\2\u008b\u008c\7"+
		"g\2\2\u008c\u008d\7v\2\2\u008d\u008e\7w\2\2\u008e\u008f\7t\2\2\u008f\u0090"+
		"\7p\2\2\u0090\22\3\2\2\2\u0091\u0092\7k\2\2\u0092\u0093\7p\2\2\u0093\u0094"+
		"\7v\2\2\u0094\24\3\2\2\2\u0095\u0096\7d\2\2\u0096\u0097\7q\2\2\u0097\u0098"+
		"\7q\2\2\u0098\u0099\7n\2\2\u0099\u009a\7g\2\2\u009a\u009b\7c\2\2\u009b"+
		"\u009c\7p\2\2\u009c\26\3\2\2\2\u009d\u009e\7k\2\2\u009e\u009f\7h\2\2\u009f"+
		"\30\3\2\2\2\u00a0\u00a1\7g\2\2\u00a1\u00a2\7n\2\2\u00a2\u00a3\7u\2\2\u00a3"+
		"\u00a4\7g\2\2\u00a4\32\3\2\2\2\u00a5\u00a6\7y\2\2\u00a6\u00a7\7j\2\2\u00a7"+
		"\u00a8\7k\2\2\u00a8\u00a9\7n\2\2\u00a9\u00aa\7g\2\2\u00aa\34\3\2\2\2\u00ab"+
		"\u00ac\7n\2\2\u00ac\u00ad\7g\2\2\u00ad\u00ae\7p\2\2\u00ae\u00af\7i\2\2"+
		"\u00af\u00b0\7v\2\2\u00b0\u00b1\7j\2\2\u00b1\36\3\2\2\2\u00b2\u00b3\7"+
		"v\2\2\u00b3\u00b4\7t\2\2\u00b4\u00b5\7w\2\2\u00b5\u00b6\7g\2\2\u00b6 "+
		"\3\2\2\2\u00b7\u00b8\7h\2\2\u00b8\u00b9\7c\2\2\u00b9\u00ba\7n\2\2\u00ba"+
		"\u00bb\7u\2\2\u00bb\u00bc\7g\2\2\u00bc\"\3\2\2\2\u00bd\u00be\7v\2\2\u00be"+
		"\u00bf\7j\2\2\u00bf\u00c0\7k\2\2\u00c0\u00c1\7u\2\2\u00c1$\3\2\2\2\u00c2"+
		"\u00c3\7p\2\2\u00c3\u00c4\7g\2\2\u00c4\u00c5\7y\2\2\u00c5&\3\2\2\2\u00c6"+
		"\u00c7\7U\2\2\u00c7\u00c8\7{\2\2\u00c8\u00c9\7u\2\2\u00c9\u00ca\7v\2\2"+
		"\u00ca\u00cb\7g\2\2\u00cb\u00cc\7o\2\2\u00cc\u00cd\7\60\2\2\u00cd\u00ce"+
		"\7q\2\2\u00ce\u00cf\7w\2\2\u00cf\u00d0\7v\2\2\u00d0\u00d1\7\60\2\2\u00d1"+
		"\u00d2\7r\2\2\u00d2\u00d3\7t\2\2\u00d3\u00d4\7k\2\2\u00d4\u00d5\7p\2\2"+
		"\u00d5\u00d6\7v\2\2\u00d6\u00d7\7n\2\2\u00d7\u00d8\7p\2\2\u00d8(\3\2\2"+
		"\2\u00d9\u00da\7?\2\2\u00da\u00db\7?\2\2\u00db*\3\2\2\2\u00dc\u00dd\7"+
		">\2\2\u00dd,\3\2\2\2\u00de\u00df\7>\2\2\u00df\u00e0\7?\2\2\u00e0.\3\2"+
		"\2\2\u00e1\u00e2\7-\2\2\u00e2\60\3\2\2\2\u00e3\u00e4\7/\2\2\u00e4\62\3"+
		"\2\2\2\u00e5\u00e6\7,\2\2\u00e6\64\3\2\2\2\u00e7\u00e8\7#\2\2\u00e8\66"+
		"\3\2\2\2\u00e9\u00ea\7(\2\2\u00ea\u00eb\7(\2\2\u00eb8\3\2\2\2\u00ec\u00ed"+
		"\7~\2\2\u00ed\u00ee\7~\2\2\u00ee:\3\2\2\2\u00ef\u00f0\7?\2\2\u00f0<\3"+
		"\2\2\2\u00f1\u00f2\7}\2\2\u00f2>\3\2\2\2\u00f3\u00f4\7\177\2\2\u00f4@"+
		"\3\2\2\2\u00f5\u00f6\7]\2\2\u00f6B\3\2\2\2\u00f7\u00f8\7_\2\2\u00f8D\3"+
		"\2\2\2\u00f9\u00fa\7*\2\2\u00faF\3\2\2\2\u00fb\u00fc\7+\2\2\u00fcH\3\2"+
		"\2\2\u00fd\u00fe\7=\2\2\u00feJ\3\2\2\2\u00ff\u0100\7.\2\2\u0100L\3\2\2"+
		"\2\u0101\u0102\7\60\2\2\u0102N\3\2\2\2\u0103\u0107\t\2\2\2\u0104\u0106"+
		"\t\3\2\2\u0105\u0104\3\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107"+
		"\u0108\3\2\2\2\u0108P\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u0113\5S*\2\u010b"+
		"\u010f\5U+\2\u010c\u010e\5S*\2\u010d\u010c\3\2\2\2\u010e\u0111\3\2\2\2"+
		"\u010f\u010d\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0113\3\2\2\2\u0111\u010f"+
		"\3\2\2\2\u0112\u010a\3\2\2\2\u0112\u010b\3\2\2\2\u0113R\3\2\2\2\u0114"+
		"\u0117\7\62\2\2\u0115\u0117\5U+\2\u0116\u0114\3\2\2\2\u0116\u0115\3\2"+
		"\2\2\u0117T\3\2\2\2\u0118\u0119\t\4\2\2\u0119V\3\2\2\2\u011a\u011b\7\61"+
		"\2\2\u011b\u011c\7\61\2\2\u011c\u0120\3\2\2\2\u011d\u011f\n\5\2\2\u011e"+
		"\u011d\3\2\2\2\u011f\u0122\3\2\2\2\u0120\u011e\3\2\2\2\u0120\u0121\3\2"+
		"\2\2\u0121\u0123\3\2\2\2\u0122\u0120\3\2\2\2\u0123\u0124\b,\2\2\u0124"+
		"X\3\2\2\2\u0125\u0126\7\61\2\2\u0126\u0127\7,\2\2\u0127\u012b\3\2\2\2"+
		"\u0128\u012a\13\2\2\2\u0129\u0128\3\2\2\2\u012a\u012d\3\2\2\2\u012b\u012c"+
		"\3\2\2\2\u012b\u0129\3\2\2\2\u012c\u012e\3\2\2\2\u012d\u012b\3\2\2\2\u012e"+
		"\u012f\7,\2\2\u012f\u0130\7\61\2\2\u0130\u0131\3\2\2\2\u0131\u0132\b-"+
		"\3\2\u0132Z\3\2\2\2\u0133\u0135\t\6\2\2\u0134\u0133\3\2\2\2\u0135\u0136"+
		"\3\2\2\2\u0136\u0134\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0138\3\2\2\2\u0138"+
		"\u0139\b.\4\2\u0139\\\3\2\2\2\n\2\u0107\u010f\u0112\u0116\u0120\u012b"+
		"\u0136\5\3,\2\3-\3\3.\4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}